package com.cp.compiler.executions.languages;

import com.cp.compiler.executions.Execution;
import com.cp.compiler.executions.ExecutionFactory;
import com.cp.compiler.models.Language;
import com.cp.compiler.models.testcases.ConvertedTestCase;
import lombok.Getter;
import lombok.val;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * The type Swift execution.
 *
 * @author Kyrylo Kotsko
 */
@Getter
public class SwiftExecution extends Execution {

    /**
     * Instantiates a new Swift Execution.
     *
     * @param sourceCodeFile the source code
     * @param testCases      the test cases
     * @param timeLimit      the time limit
     * @param memoryLimit    the memory limit
     */
    public SwiftExecution(MultipartFile sourceCodeFile,
                          List<ConvertedTestCase> testCases,
                          int timeLimit,
                          int memoryLimit) {
        super(sourceCodeFile, testCases, timeLimit, memoryLimit, ExecutionFactory.getExecutionType(Language.SWIFT));
    }

    @Override
    protected Map<String, String> getParameters(String inputFileName) {
        val compiledFile = "main";
        val commandPrefix = "./" + compiledFile;
        val executionCommand = inputFileName == null ? commandPrefix + "\n" : commandPrefix + " < " + inputFileName + "\n";
        return Map.of(
                "timeLimit", String.valueOf(getTimeLimit()),
                "memoryLimit", String.valueOf(getMemoryLimit()),
                "executionCommand", executionCommand);
    }

    @Override
    public Language getLanguage() {
        return Language.SWIFT;
    }

    @Override
    protected void copyLanguageSpecificFilesToExecutionDirectory() throws IOException {
        // Empty
    }
}
