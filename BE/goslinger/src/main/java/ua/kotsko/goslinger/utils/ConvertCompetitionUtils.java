package ua.kotsko.goslinger.utils;

import ua.kotsko.goslinger.dto.competition.TournamentInfo;
import ua.kotsko.goslinger.entity.Tournament;

public class ConvertCompetitionUtils extends ConvertUtils{

    public static TournamentInfo convertToTournamentInfo(Tournament tournament) {
        return TournamentInfo.builder()
                .id(tournament.getId())
                .title(tournament.getTitle())
                .description(tournament.getDescription())
                .isPublic(tournament.isPublic())
                .tasks(tournament.getTasks().size())
                .participants(tournament.getParticipants().size())
                .maxParticipants(tournament.getMaxParticipants())
                .startTime(convertToStringDate(tournament.getStartTime()))
                .endTime(convertToStringDate(tournament.getEndTime()))
                .build();
    }

}
