package ua.kotsko.goslinger.dto.submission;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubmissionRow {

    String id;
    String createdTime;
    String language;
    int mark;
    String taskTittle;
    String tournamentTitle;


}
