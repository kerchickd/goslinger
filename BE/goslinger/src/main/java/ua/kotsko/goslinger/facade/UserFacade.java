package ua.kotsko.goslinger.facade;

import ua.kotsko.goslinger.dto.user.*;

public interface UserFacade {

    ProfileDTO getCurrentUserProfile();

    ProfileDTO getUserProfile(Long id);

    ProfileDTO updateUserFull(UpdateUserRequest updateUserRequest);

    ProfileDTO updateCurrentUser(UpdateCurrentUserRequest currentUserRequest);

    RatingResponse getUserRatingList(int page, int size);

    UserPageResponse searchUserByText(String searchText, int page, int size);
}
