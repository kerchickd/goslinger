package ua.kotsko.goslinger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;


@RequiredArgsConstructor
public enum Role {
    USER("USER"),
    ADMIN("ADMIN");

    @Getter
    private final String authority;

    public String getWrappedAuthority() {
        return "ROLE_" + this.authority;
    }

    public Collection<GrantedAuthority> getAuthorityRole() {
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(this.getWrappedAuthority());
        return List.of(grantedAuthority);
    }

}
