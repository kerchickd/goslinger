package ua.kotsko.goslinger.dto.tournament;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TournamentRowResponse {

    private Long id;
    private String title;
    private boolean isPublic;
    private int participants;
    private int maxParticipants;
    private int amountTasks;
    private String startTime;
    private String endTime;

}
