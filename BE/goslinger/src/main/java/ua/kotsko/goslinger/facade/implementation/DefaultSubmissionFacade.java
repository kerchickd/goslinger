package ua.kotsko.goslinger.facade.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionList;
import ua.kotsko.goslinger.entity.Submission;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.facade.SubmissionFacade;
import ua.kotsko.goslinger.service.SubmissionService;
import ua.kotsko.goslinger.service.UserService;
import ua.kotsko.goslinger.utils.ConvertSubmissionUtils;

@Component
@RequiredArgsConstructor
public class DefaultSubmissionFacade implements SubmissionFacade {

    private final SubmissionService submissionService;
    private final UserService userService;

    @Override
    public SubmissionList getAllCurrentUserSubmissions() {
        User user = userService.getCurrentUser();
        return ConvertSubmissionUtils.convertToSubmissionList(user.getSubmissions());
    }

    @Override
    public SubmissionList getAllUserSubmissions(Long userId) {
        User user = userService.getUserById(userId);
        return ConvertSubmissionUtils.convertToSubmissionList(user.getSubmissions());
    }

    @Override
    public SubmissionDTO getSubmission(String submissionId) {
        Submission submission = submissionService.getSubmission(submissionId);
        return ConvertSubmissionUtils.convertToSubmissionDTO(submission);
    }

}
