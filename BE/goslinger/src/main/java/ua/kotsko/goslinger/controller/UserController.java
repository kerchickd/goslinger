package ua.kotsko.goslinger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ua.kotsko.goslinger.dto.user.*;
import ua.kotsko.goslinger.facade.UserFacade;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserFacade userFacade;

    @GetMapping("/current/profile")
    public ProfileDTO getCurrentUserProfile() {
        return userFacade.getCurrentUserProfile();
    }

    @GetMapping("/view/{id}/profile")
    public ProfileDTO getUserProfile(@PathVariable Long id) {
        return userFacade.getUserProfile(id);
    }

    @PutMapping("/current/profile/update")
    public ProfileDTO updateCurrentUser(@RequestBody UpdateCurrentUserRequest userRequest) {
        return userFacade.updateCurrentUser(userRequest);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/view/{id}/profile/update")
    public ProfileDTO updateUserFull(@PathVariable Long id,
                                     @RequestBody UpdateUserRequest userRequest) {
        userRequest.setId(id);
        return userFacade.updateUserFull(userRequest);
    }

    @GetMapping("/rating")
    public RatingResponse getRatingPageList(@RequestParam int page,
                                            @RequestParam int size) {
        return userFacade.getUserRatingList(page, size);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/search")
    public UserPageResponse searchUserByText(@RequestParam(defaultValue = "") String searchText,
                                             @RequestParam int page,
                                             @RequestParam int size) {
        return userFacade.searchUserByText(searchText, page, size);
    }

}
