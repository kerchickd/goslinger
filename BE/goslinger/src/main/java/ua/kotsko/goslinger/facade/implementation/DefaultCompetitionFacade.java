package ua.kotsko.goslinger.facade.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.dto.competition.*;
import ua.kotsko.goslinger.dto.compiler.CodeDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;
import ua.kotsko.goslinger.entity.Submission;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.facade.CompetitionFacade;
import ua.kotsko.goslinger.service.*;
import ua.kotsko.goslinger.service.implementation.CompilerMicroserviceConnection;
import ua.kotsko.goslinger.utils.ConvertCompetitionUtils;
import ua.kotsko.goslinger.utils.ConvertSubmissionUtils;
import ua.kotsko.goslinger.utils.ConvertTournamentUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class DefaultCompetitionFacade implements CompetitionFacade {

    private static final String HEADER_USER = "user";
    private static final String HEADER_TOTAL = "total";

    private final UserService userService;
    private final TournamentService tournamentService;
    private final TaskService taskService;
    private final CompetitionService competitionService;
    private final SubmissionService submissionService;
    private final CompilerMicroserviceConnection compilerMicroserviceConnection;

    @Override
    public String participate(Long tournamentId, PasswordRequest passwordRequest) {
        User user = userService.getCurrentUser();
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        return competitionService.tryToParticipate(user, tournament, passwordRequest.getPassword());
    }

    @Override
    public TournamentPageResponse searchTournamentAvailable(String searchText, int page, int size) {
        User user = userService.getCurrentUser();
        Page<Tournament> tournamentPage = competitionService.searchTournamentAvailable(searchText,
                page,
                size,
                user);
        return ConvertTournamentUtils.convertToTournamentPage(tournamentPage);
    }

    @Override
    public TournamentPageResponse searchTournamentParticipated(String searchText, int page, int size) {
        User user = userService.getCurrentUser();
        Page<Tournament> tournamentPage = competitionService.searchTournamentParticipated(searchText,
                page,
                size,
                user);
        return ConvertTournamentUtils.convertToTournamentPage(tournamentPage);
    }

    @Override
    public TournamentPageResponse searchTournamentFinished(String searchText, int page, int size) {
        Page<Tournament> tournamentPage = competitionService.searchTournamentFinished(searchText,
                page,
                size);
        return ConvertTournamentUtils.convertToTournamentPage(tournamentPage);
    }

    @Override
    public TournamentInfo getTournamentInfo(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        return ConvertCompetitionUtils.convertToTournamentInfo(tournament);
    }

    @Override
    public TournamentTasks getTournamentTasks(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        List<TaskInfo> tasks = tournament.getTasks().stream()
                .sorted(Comparator.comparingLong(Task::getId))
                .map(x -> createTaskInfo(x, tournament))
                .toList();
        List<String> alphabet = submissionService.generateAlphabet(tasks.size());
        IntStream.range(0, alphabet.size())
                .forEach(i -> tasks.get(i).setLetter(alphabet.get(i)));
        return TournamentTasks.builder()
                .tasks(tasks)
                .build();
    }

    @Override
    public TournamentResults getTournamentResults(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        return TournamentResults.builder()
                .headers(createHeaders(tournament.getTasks().size()))
                .body(createBody(tournament))
                .build();
    }

    @Override
    public SubmissionDTO createAndSendRequestToCompiler(Long tournamentId, Long taskId, CodeDTO codeDTO) {
        User user = userService.getCurrentUser();
        Task task = taskService.getTaskById(taskId);
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        Submission submission = submissionService.createSubmission(task, tournament, codeDTO);
        compilerMicroserviceConnection.sendRequest(submission);
        return ConvertSubmissionUtils.convertToSubmissionDTO(submission);
    }

    private List<BodyResult> createBody(Tournament tournament) {
        Set<Task> tasks = tournament.getTasks();
        Set<User> participants = tournament.getParticipants();
        Set<Submission> submissions = tournament.getSubmissions();
        return participants.stream()
                .map(user -> createBodyResult(user, tasks, submissions))
                .sorted(Comparator.comparingInt(BodyResult::getTotalMark).reversed())
                .toList();
    }

    private BodyResult createBodyResult(User user, Set<Task> tasks, Set<Submission> submissions) {
        List<Integer> marks = tasks.stream()
                .sorted(Comparator.comparingLong(Task::getId))
                .map(task -> submissionService.getMaxMark(task, user, submissions))
                .toList();
        return BodyResult.builder()
                .id(user.getId())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .icon(user.getIcon().getName())
                .marks(marks)
                .totalMark(marks.stream().reduce(0, Integer::sum))
                .build();
    }

    private List<String> createHeaders(int size) {
        List<String> alphabet = submissionService.generateAlphabet(size);
        List<String> headers = new ArrayList<>();
        headers.add(HEADER_USER);
        headers.add(HEADER_TOTAL);
        headers.addAll(alphabet);
        return headers;
    }

    private TaskInfo createTaskInfo(Task task, Tournament tournament) {
        User user = userService.getCurrentUser();
        String color = getTaskColor(task, user, tournament.getSubmissions());
        return TaskInfo.builder()
                .id(task.getId())
                .title(task.getTitle())
                .difficulty(task.getDifficulty().getName())
                .color(color)
                .build();
    }

    private String getTaskColor(Task task, User user, Set<Submission> submissions) {
        int mark = submissionService.getMaxMark(task, user, submissions);
        if (mark == 100) {
            return "lightgreen";
        } else if (mark > 50) {
            return "yellow";
        } else if (mark > 0) {
            return "red";
        }
        return "white";
    }

}
