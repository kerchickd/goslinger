package ua.kotsko.goslinger.exception;

public class TaskNotContainInTournament extends Exception{
    public TaskNotContainInTournament(String msg) {
        super(msg);
    }
}
