package ua.kotsko.goslinger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoslingerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoslingerApplication.class, args);
	}

}
