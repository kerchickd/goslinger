package ua.kotsko.goslinger.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.kotsko.goslinger.dto.compiler.CodeDTO;
import ua.kotsko.goslinger.dto.compiler.CodeResponse;
import ua.kotsko.goslinger.entity.*;
import ua.kotsko.goslinger.repository.SubmissionRepository;
import ua.kotsko.goslinger.repository.UserRepository;
import ua.kotsko.goslinger.service.SubmissionService;
import ua.kotsko.goslinger.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class DefaultSubmissionService implements SubmissionService {

    private final SubmissionRepository submissionRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final Map<Difficulty, Integer> coefficientMap;

    @Override
    public Submission getSubmission(String id) {
        return submissionRepository.getReferenceById(id);
    }

    @Override
    public int getMaxMark(Task task, User user, Set<Submission> submissions) {
        return submissions.stream()
                .filter(submission ->
                                submission.getTask().equals(task) &&
                                submission.getUser().equals(user) &&
                                submission.getResult() != null)
                .mapToInt(submission -> submission.getResult().getMark())
                .max()
                .orElse(0);
    }

    @Override
    public List<String> generateAlphabet(int size) {
        List<String> alphabet = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < size; i++) {
            if (i < 26) {
                sb.setLength(0);
                sb.append((char) ('A' + i));
                alphabet.add(sb.toString());
            } else {
                int quotient = i / 26;
                int remainder = i % 26;

                sb.setLength(0);
                sb.append((char) ('A' + quotient - 1));
                sb.append((char) ('A' + remainder));
                alphabet.add(sb.toString());
            }
        }

        return alphabet;
    }

    @Override
    public Submission createSubmission(Task task, Tournament tournament, CodeDTO codeDTO) {
        User user = userService.getCurrentUser();
        Submission submission = Submission.builder()
                .task(task)
                .tournament(tournament)
                .user(user)
                .language(codeDTO.getLanguage())
                .sourceCode(codeDTO.getCode())
                .build();
        submission = submissionRepository.save(submission);
        return submission;
    }

    @Override
    public void saveResult(Submission submission, CodeResponse response, int maxMark) {
        User user = submission.getUser();
        Task task = submission.getTask();
        Result result = buildResult(submission, response);
        updateUserInformation(user, task, maxMark, result);
        submission.setResult(result);
        submissionRepository.save(submission);
    }

    private void updateUserInformation(User user, Task task, int maxMark, Result result) {
        int newMark = result.getMark();
        if (newMark > maxMark) {
            int pointsToAdd = (newMark - maxMark) * coefficientMap.get(task.getDifficulty());
            int points = user.getInformation().getPoints() + pointsToAdd;
            user.getInformation().setPoints(points);
            if (newMark == 100) {
                user.getInformation().setSolvedTasks(user.getInformation().getSolvedTasks() + 1);
            }
        }
        userRepository.save(user);
    }

    private Result buildResult(Submission submission, CodeResponse response) {
        return Result.builder()
                .submission(submission)
                .status(response.getStatusCode())
                .compilationDuration(response.getCompilationDuration())
                .averageExecutionDuration(response.getAverageExecutionDuration())
                .error(response.getError())
                .mark(calculateMark(response, submission))
                .verdict(response.getVerdict())
                .build();
    }

    private int calculateMark(CodeResponse response, Submission submission) {
        if (response.getStatusCode() == 100) {
            return 100;
        }
        int passedTestCases = response.getTestCasesResult().entrySet().size() - 1;
        int testCasesAmount = submission.getTask().getTestCaseList().size();
        return 100 * passedTestCases / testCasesAmount;
    }

}
