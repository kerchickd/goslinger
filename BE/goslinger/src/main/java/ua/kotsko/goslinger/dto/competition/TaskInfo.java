package ua.kotsko.goslinger.dto.competition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskInfo {

    String letter;
    Long id;
    String title;
    String difficulty;
    String color;

}
