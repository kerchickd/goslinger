package ua.kotsko.goslinger.dto.compiler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.kotsko.goslinger.entity.Language;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CodeDTO {

    String code;
    Language language;

}
