package ua.kotsko.goslinger.dto.compiler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.kotsko.goslinger.entity.Language;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CodeResponse {

    String verdict;
    int statusCode;
    String error;
    LinkedHashMap<String, TestCaseResult> testCasesResult;
    int compilationDuration;
    float averageExecutionDuration;
    int timeLimit;
    int memoryLimit;
    Language language;
    LocalDateTime dateTime;

}
