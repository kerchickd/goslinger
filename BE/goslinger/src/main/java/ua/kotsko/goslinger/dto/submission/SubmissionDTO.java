package ua.kotsko.goslinger.dto.submission;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubmissionDTO {

    String id;
    String createdTime;
    String sourceCode;
    String language;
    String icon;
    String firstname;
    String lastname;
    int mark;
    int status;
    float averageExecutionDuration;
    String error;
    String taskTittle;
    String tournamentTitle;
    String verdict;

}
