package ua.kotsko.goslinger.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;

@Repository
public interface TournamentRepository extends JpaRepository<Tournament, Long> {

    @Query(value = "SELECT t " +
            "FROM Tournament t " +
            "WHERE (t.title LIKE %:searchText% " +
            "OR t.description LIKE %:searchText%) " +
            "AND t.endTime > CURRENT_TIMESTAMP " +
            "ORDER BY t.id",
            countQuery = "SELECT COUNT(t) " +
                    "FROM Tournament t " +
                    "WHERE (t.title LIKE %:searchText% " +
                    "OR t.description LIKE %:searchText%) " +
                    "AND t.endTime > CURRENT_TIMESTAMP ")
    Page<Tournament> searchPagingTournaments(@Param("searchText") String searchText, PageRequest pageRequest);

    @Query(value = "SELECT t " +
            "FROM Tournament t " +
            "WHERE (t.title LIKE %:searchText% " +
            "OR t.description LIKE %:searchText%) " +
            "AND t.endTime > CURRENT_TIMESTAMP " +
            "AND NOT EXISTS (SELECT 1 FROM t.participants p WHERE p = :user) " +
            "ORDER BY t.id",
            countQuery = "SELECT COUNT(t) " +
                    "FROM Tournament t " +
                    "WHERE (t.title LIKE %:searchText% " +
                    "OR t.description LIKE %:searchText%) " +
                    "AND t.endTime > CURRENT_TIMESTAMP " +
                    "AND NOT EXISTS (SELECT 1 FROM t.participants p WHERE p = :user) ")
    Page<Tournament> searchAvailableTournaments(@Param("searchText") String searchText,
                                                @Param("user") User user,
                                                PageRequest pageRequest);

    @Query(value = "SELECT t " +
            "FROM Tournament t " +
            "WHERE (t.title LIKE %:searchText% " +
            "OR t.description LIKE %:searchText%) " +
            "AND t.endTime > CURRENT_TIMESTAMP " +
            "AND EXISTS (SELECT 1 FROM t.participants p WHERE p = :user) " +
            "ORDER BY t.id",
            countQuery = "SELECT COUNT(t) " +
                    "FROM Tournament t " +
                    "WHERE (t.title LIKE %:searchText% " +
                    "OR t.description LIKE %:searchText%) " +
                    "AND t.endTime > CURRENT_TIMESTAMP " +
                    "AND EXISTS (SELECT 1 FROM t.participants p WHERE p = :user) ")
    Page<Tournament> searchParticipatedTournaments(@Param("searchText") String searchText,
                                                @Param("user") User user,
                                                PageRequest pageRequest);

    @Query(value = "SELECT t " +
            "FROM Tournament t " +
            "WHERE (t.title LIKE %:searchText% " +
            "OR t.description LIKE %:searchText%) " +
            "AND t.endTime < CURRENT_TIMESTAMP " +
            "ORDER BY t.id",
            countQuery = "SELECT COUNT(t) " +
                    "FROM Tournament t " +
                    "WHERE (t.title LIKE %:searchText% " +
                    "OR t.description LIKE %:searchText%) " +
                    "AND t.endTime < CURRENT_TIMESTAMP ")
    Page<Tournament> searchFinishedTournaments(@Param("searchText") String searchText,
                                                   PageRequest pageRequest);
}
