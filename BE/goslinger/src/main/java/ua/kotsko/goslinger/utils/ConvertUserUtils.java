package ua.kotsko.goslinger.utils;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.dto.user.*;
import ua.kotsko.goslinger.entity.Rating;
import ua.kotsko.goslinger.entity.User;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ConvertUserUtils extends ConvertUtils {

    public static Integer calculateAge(Date birthayDate) {
        LocalDate birthdayLocalDate = birthayDate
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        return Period.between(birthdayLocalDate, LocalDate.now()).getYears();
    }

    public static ProfileDTO convertToProfileDTO(User user) {
        return ProfileDTO.builder()
                .email(user.getEmail())
                .icon(user.getIcon().getName())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .points(user.getInformation().getPoints())
                .solvedTasks(user.getInformation().getSolvedTasks())
                .tournamentParticipation(user.getInformation().getTournamentParticipation())
                .age(calculateAge(user.getInformation().getBirthayDate()))
                .role(user.getRole().name())
                .birthdayDate(user.getInformation().getBirthayDate())
                .isEnabled(user.isEnabled())
                .build();
    }

    public static RatingResponse convertToRatingResponse(Page<Rating> ratingPage) {
        return RatingResponse.builder()
                .pageInfo(convertToPageInfo(ratingPage))
                .ratingList(convertToRatingList(ratingPage.getContent()))
                .build();
    }

    public static List<RatingRowResponse> convertToRatingList(List<Rating> content) {
        return content.stream()
                .map(ConvertUserUtils::convertToRatingRowResponse)
                .collect(Collectors.toList());
    }

    public static RatingRowResponse convertToRatingRowResponse(Rating rating) {
        return RatingRowResponse.builder()
                .position(rating.getPosition())
                .id(rating.getUser().getId())
                .firstname(rating.getUser().getFirstname())
                .lastname(rating.getUser().getLastname())
                .age(calculateAge(rating.getUser().getInformation().getBirthayDate()))
                .points(rating.getUser().getInformation().getPoints())
                .solvedTasks(rating.getUser().getInformation().getSolvedTasks())
                .tournamentParticipation(rating.getUser().getInformation().getTournamentParticipation())
                .icon(rating.getUser().getIcon().getName())
                .build();
    }

    public static UserPageResponse convertToUserPageResponse(Page<User> userPage) {
        return UserPageResponse.builder()
                .pageInfo(convertToPageInfo(userPage))
                .userRows(convertToUserRows(userPage.getContent()))
                .build();
    }

    public static List<UserRowResponse> convertToUserRows(List<User> content) {
        return content.stream()
                .map(ConvertUserUtils::convertToUserRowResponse)
                .collect(Collectors.toList());
    }

    public static List<UserRowResponse> convertToUserRows(Set<User> content) {
        return content.stream()
                .map(ConvertUserUtils::convertToUserRowResponse)
                .collect(Collectors.toList());
    }

    public static UserRowResponse convertToUserRowResponse(User user) {
        return UserRowResponse.builder()
                .isEnabled(user.isEnabled())
                .age(calculateAge(user.getInformation().getBirthayDate()))
                .role(user.getRole().name())
                .email(user.getEmail())
                .id(user.getId())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .icon(user.getIcon().getName())
                .build();
    }

    public static ParticipantList convertToParticipantList(Set<User> participants) {
        return ParticipantList.builder()
                .participants(ConvertUserUtils.convertToUserRows(participants))
                .build();
    }

}
