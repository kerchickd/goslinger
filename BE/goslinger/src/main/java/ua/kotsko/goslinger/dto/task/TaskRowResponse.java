package ua.kotsko.goslinger.dto.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskRowResponse {

    private Long id;
    private String title;
    private Integer timeLimit;
    private Integer memoryLimit;
    private Integer testCasesAmount;
    private String difficulty;

}
