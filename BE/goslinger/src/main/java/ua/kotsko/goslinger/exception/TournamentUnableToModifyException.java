package ua.kotsko.goslinger.exception;

public class TournamentUnableToModifyException extends Exception {
    public TournamentUnableToModifyException(String msg) {
        super(msg);
    }
}
