package ua.kotsko.goslinger.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicInsert
@ToString(exclude = {"tasks", "participants", "submissions"})
@EqualsAndHashCode(exclude = {"tasks", "participants", "submissions"})
@Table(name = "tournaments")
public class Tournament {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    private Date startTime;

    private Date endTime;

    private boolean isPublic;

    private String password;

    private int maxParticipants;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(joinColumns = @JoinColumn(name = "tournament_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private Set<Task> tasks = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(joinColumns = @JoinColumn(name = "tournament_id"),
            inverseJoinColumns = @JoinColumn(name = "participants_id"))
    private Set<User> participants = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "tournament",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Submission> submissions = new HashSet<>();

}
