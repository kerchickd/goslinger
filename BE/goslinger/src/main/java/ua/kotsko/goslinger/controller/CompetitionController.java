package ua.kotsko.goslinger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.kotsko.goslinger.dto.competition.*;
import ua.kotsko.goslinger.dto.compiler.CodeDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;
import ua.kotsko.goslinger.facade.CompetitionFacade;
import ua.kotsko.goslinger.facade.TaskFacade;
import ua.kotsko.goslinger.validation.CompetitionValidation;

@RestController
@RequestMapping("/api/competition")
@RequiredArgsConstructor
public class CompetitionController {

    private final CompetitionFacade competitionFacade;
    private final CompetitionValidation competitionValidation;
    private final TaskFacade taskFacade;

    @GetMapping("/{id}")
    public TournamentInfo getTournamentInfo(@PathVariable Long id) {
        competitionValidation.validateAccess(id);
        return competitionFacade.getTournamentInfo(id);
    }

    @GetMapping("/{id}/tasks")
    public TournamentTasks getTournamentTasks(@PathVariable Long id) {
        competitionValidation.validateAccess(id);
        return competitionFacade.getTournamentTasks(id);
    }

    @GetMapping("/{id}/results")
    public TournamentResults getTournamentResults(@PathVariable Long id) {
        competitionValidation.validateAccess(id);
        return competitionFacade.getTournamentResults(id);
    }

    @PostMapping("/{id}/participate")
    public String participate(@PathVariable Long id,
                              @RequestBody PasswordRequest passwordRequest) {
        return competitionFacade.participate(id, passwordRequest);
    }

    @GetMapping("/available/search")
    public TournamentPageResponse searchTournamentAvailable(@RequestParam(defaultValue = "") String searchText,
                                                         @RequestParam int page,
                                                         @RequestParam int size) {
        return competitionFacade.searchTournamentAvailable(searchText, page, size);
    }

    @GetMapping("/participated/search")
    public TournamentPageResponse searchTournamentParticipated(@RequestParam(defaultValue = "") String searchText,
                                                            @RequestParam int page,
                                                            @RequestParam int size) {
        return competitionFacade.searchTournamentParticipated(searchText, page, size);
    }

    @GetMapping("/finished/search")
    public TournamentPageResponse searchTournamentFinished(@RequestParam(defaultValue = "") String searchText,
                                                               @RequestParam int page,
                                                               @RequestParam int size) {
        return competitionFacade.searchTournamentFinished(searchText, page, size);
    }

    @PostMapping("/tournament/{tournamentId}/task/{taskId}/submission/create")
    public SubmissionDTO createAndSendSubmission(@PathVariable Long tournamentId,
                                                 @PathVariable Long taskId,
                                                 @RequestBody CodeDTO codeDTO) {
        competitionValidation.validateSendRequest(tournamentId, taskId);
        return competitionFacade.createAndSendRequestToCompiler(tournamentId, taskId, codeDTO);
    }

    @GetMapping("/tournament/{tournamentId}/task/{taskId}")
    public TaskDTO getTaskDTO(@PathVariable Long tournamentId,
                              @PathVariable Long taskId) {
        competitionValidation.validateSendRequest(tournamentId, taskId);
        return taskFacade.getTaskById(taskId);
    }

}
