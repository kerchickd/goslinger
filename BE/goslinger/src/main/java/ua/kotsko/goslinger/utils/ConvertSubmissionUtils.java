package ua.kotsko.goslinger.utils;

import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionList;
import ua.kotsko.goslinger.dto.submission.SubmissionRow;
import ua.kotsko.goslinger.entity.Result;
import ua.kotsko.goslinger.entity.Submission;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class ConvertSubmissionUtils extends ConvertUtils{

    public static SubmissionList convertToSubmissionList(Set<Submission> submissions) {
        List<SubmissionRow> submissionRowList = submissions.stream()
                .sorted(Comparator.comparing(Submission::getCreatedTime).reversed())
                .map(ConvertSubmissionUtils::convertToSubmissionRow)
                .toList();
        return SubmissionList.builder()
                .submissions(submissionRowList)
                .build();
    }

    public static SubmissionRow convertToSubmissionRow(Submission submission) {
        int mark = submission.getResult() != null ?
                submission.getResult().getMark() :
                -1;
        return SubmissionRow.builder()
                .id(submission.getId())
                .createdTime(convertToStringDate(submission.getCreatedTime()))
                .language(submission.getLanguage().getName())
                .mark(mark)
                .taskTittle(submission.getTask().getTitle())
                .tournamentTitle(submission.getTournament().getTitle())
                .build();
    }

    public static SubmissionDTO convertToSubmissionDTO(Submission submission) {
        int mark = -1;
        int status = -1;
        float averageExecutionDuration = 0.0f;
        String error = "";
        String verdict = "Checking...";

        Result submissionResult = submission.getResult();
        if (submissionResult != null) {
            mark = submissionResult.getMark();
            status = submissionResult.getStatus();
            averageExecutionDuration = submissionResult.getAverageExecutionDuration();
            error = submissionResult.getError();
            verdict = submissionResult.getVerdict();
        }

        return SubmissionDTO.builder()
                .id(submission.getId())
                .createdTime(convertToStringDate(submission.getCreatedTime()))
                .language(submission.getLanguage().getName())
                .sourceCode(submission.getSourceCode())
                .taskTittle(submission.getTask().getTitle())
                .tournamentTitle(submission.getTournament().getTitle())
                .icon(submission.getUser().getIcon().getName())
                .firstname(submission.getUser().getFirstname())
                .lastname(submission.getUser().getLastname())
                .mark(mark)
                .status(status)
                .averageExecutionDuration(averageExecutionDuration)
                .verdict(verdict)
                .error(error)
                .build();
    }
}
