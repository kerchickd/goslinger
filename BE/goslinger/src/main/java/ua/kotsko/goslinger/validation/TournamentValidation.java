package ua.kotsko.goslinger.validation;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.exception.TournamentUnableToModifyException;
import ua.kotsko.goslinger.service.TournamentService;

import java.util.Date;

@Component
@RequiredArgsConstructor
public class TournamentValidation {

    private final TournamentService tournamentService;

    @SneakyThrows
    public void validateTime(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        if (tournament.getEndTime().before(new Date(System.currentTimeMillis()))) {
            throw new TournamentUnableToModifyException("Tournament has already finished!");
        }
    }

}
