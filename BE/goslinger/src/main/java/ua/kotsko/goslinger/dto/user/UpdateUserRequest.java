package ua.kotsko.goslinger.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserRequest {

    private Long id;
    private String firstname;
    private String lastname;
    private Date birthdayDate;
    private boolean changeRoleToAdmin;
    private boolean isEnabled;

}
