package ua.kotsko.goslinger.facade;

import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionList;

public interface SubmissionFacade {
    SubmissionList getAllCurrentUserSubmissions();

    SubmissionList getAllUserSubmissions(Long userId);

    SubmissionDTO getSubmission(String submissionId);
}
