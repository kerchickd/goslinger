package ua.kotsko.goslinger.exception;

public class ForbiddenAccessException extends Exception {

    public ForbiddenAccessException(String msg) {
        super(msg);
    }

}
