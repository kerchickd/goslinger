package ua.kotsko.goslinger.facade;

import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.dto.task.TaskPageResponse;

public interface TaskFacade {

    TaskDTO createOrUpdateTask(TaskDTO taskDTO);

    TaskDTO getTaskById(Long id);

    TaskPageResponse searchTaskByText(String searchText, int page, int size);
}
