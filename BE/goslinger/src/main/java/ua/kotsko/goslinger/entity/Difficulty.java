package ua.kotsko.goslinger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Difficulty {

    HIGH("HIGH"),
    MEDIUM("MEDIUM"),
    LOW("LOW");

    @Getter
    private final String name;

}
