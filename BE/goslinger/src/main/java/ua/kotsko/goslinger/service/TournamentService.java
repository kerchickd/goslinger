package ua.kotsko.goslinger.service;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.dto.tournament.TournamentDTO;
import ua.kotsko.goslinger.entity.Tournament;

public interface TournamentService {
    Tournament getTournamentById(Long id);

    Tournament createOrUpdateTournament(TournamentDTO tournamentDTO);

    Page<Tournament> searchTournamentByText(String searchText, int page, int size);

    void addTask(Long id, Long taskId);

    void removeTask(Long id, Long taskId);

    void removeParticipant(Long id, Long participantId);
}
