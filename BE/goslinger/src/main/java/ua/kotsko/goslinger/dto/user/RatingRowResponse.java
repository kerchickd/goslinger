package ua.kotsko.goslinger.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RatingRowResponse {

    private long position;
    private long id;
    private String firstname;
    private String lastname;
    private String icon;
    private int age;
    private int points;
    private int solvedTasks;
    private int tournamentParticipation;

}
