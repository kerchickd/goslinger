package ua.kotsko.goslinger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Language {

    PYTHON("PYTHON"),
    C("C"),
    CPP("CPP"),
    JAVA("JAVA"),
    GO("GO"),
    CS("CS"),
    KOTLIN("KOTLIN"),
    SCALA("SCALA"),
    RUST("RUST"),
    RUBY("RUBY"),
    HASKELL("HASKELL"),
    SWIFT("SWIFT");

    @Getter
    private final String name;

}
