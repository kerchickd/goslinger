package ua.kotsko.goslinger.service.implementation;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ua.kotsko.goslinger.dto.compiler.CodeRequest;
import ua.kotsko.goslinger.dto.compiler.CodeResponse;
import ua.kotsko.goslinger.dto.compiler.TestCaseDTO;
import ua.kotsko.goslinger.entity.Submission;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.entity.TestCase;
import ua.kotsko.goslinger.service.SubmissionService;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class CompilerMicroserviceConnection {

    public static final String URL = "http://localhost:8082/api/compile/json/";

    private final RestTemplate restTemplate;
    private final SubmissionService submissionService;

    public void sendRequest(Submission submission) {
        Task task = submission.getTask();
        CodeRequest body = CodeRequest.builder()
                .language(submission.getLanguage())
                .sourcecode(submission.getSourceCode())
                .timeLimit(task.getTimeLimit())
                .memoryLimit(task.getMemoryLimit())
                .testCases(populateTestCases(task))
                .build();
        Set<Submission> submissions = submission.getTournament().getSubmissions();
        int maxMark = submissionService.getMaxMark(task, submission.getUser(), submissions);
        new Thread(() -> {
            CodeResponse codeResponse = send(body);
            submissionService.saveResult(submission, codeResponse, maxMark);
        }).start();
    }

    private CodeResponse send(CodeRequest body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CodeRequest> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<CodeResponse> responseEntity = restTemplate.exchange(
                URL,
                HttpMethod.POST,
                requestEntity,
                CodeResponse.class
        );
        return responseEntity.getBody();
    }

    private LinkedHashMap<String, TestCaseDTO> populateTestCases(Task task) {
        LinkedHashMap<String, TestCaseDTO> result = new LinkedHashMap<>();
        for (TestCase testCase: task.getTestCaseList()) {
            TestCaseDTO testCaseDTO = TestCaseDTO.builder()
                    .input(testCase.getInputData())
                    .expectedOutput(testCase.getOutputData())
                    .build();
            result.put(testCase.getId(), testCaseDTO);
        }
        return result;
    }
}
