package ua.kotsko.goslinger.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString(exclude = {"example", "testCaseList", "tournaments", "submissions"})
@EqualsAndHashCode(exclude = {"example", "testCaseList", "tournaments", "submissions"})
@DynamicInsert
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private Integer timeLimit;
    private Integer memoryLimit;
    @Column(columnDefinition = "TEXT")
    private String problemStatement;
    @Column(columnDefinition = "TEXT")
    private String inputText;
    @Column(columnDefinition = "TEXT")
    private String outputText;
    @Enumerated(EnumType.STRING)
    private Difficulty difficulty;
    @OneToOne(mappedBy = "task",
            orphanRemoval = true,
            cascade = CascadeType.ALL)
    private Example example;
    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "task",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<TestCase> testCaseList;
    @ManyToMany(fetch = FetchType.LAZY,
            mappedBy = "tasks",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Tournament> tournaments = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "task",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Submission> submissions = new HashSet<>();

    public void addExample(String inputData, String outputData) {
        this.example = Example.builder()
                .inputData(inputData)
                .outputData(outputData)
                .task(this)
                .build();
    }

    public void addTestCase(String inputData, String outputData) {
        if (this.testCaseList == null)
            this.testCaseList = new ArrayList<>();
        TestCase testCase = TestCase.builder()
                .inputData(inputData)
                .outputData(outputData)
                .task(this)
                .build();
        this.testCaseList.add(testCase);
    }

}
