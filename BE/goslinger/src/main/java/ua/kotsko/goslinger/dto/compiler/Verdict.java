package ua.kotsko.goslinger.dto.compiler;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class Verdict {

    String statusResponse;
    int statusCode;
    String counterMetric;

}
