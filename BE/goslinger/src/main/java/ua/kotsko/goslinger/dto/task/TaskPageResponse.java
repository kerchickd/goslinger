package ua.kotsko.goslinger.dto.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.kotsko.goslinger.dto.PageResponse;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskPageResponse {

    private PageResponse pageInfo;
    private List<TaskRowResponse> taskRows;
}
