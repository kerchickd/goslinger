package ua.kotsko.goslinger.controller;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.dto.task.TaskPageResponse;
import ua.kotsko.goslinger.facade.TaskFacade;
import ua.kotsko.goslinger.service.TaskService;

@RestController
@RequestMapping("/api/task")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequiredArgsConstructor
public class TaskController {

    public static final String TEXT_CSV = "text/csv";
    public static final String CONTENT_DISPOSITION_HEADER = "Content-Disposition";
    public static final String CONTENT_DISPOSITION_VALUE = "attachment; filename=\"testcases.csv\"";
    private final TaskFacade taskFacade;
    private final TaskService taskService;

    @GetMapping("/{id}")
    public TaskDTO getTaskById(@PathVariable Long id) {
        return taskFacade.getTaskById(id);
    }

    @PostMapping("/create")
    public TaskDTO createTask(@RequestBody TaskDTO taskDTO) {
        return taskFacade.createOrUpdateTask(taskDTO);
    }

    @PutMapping("/{id}/update")
    public TaskDTO updateTask(@PathVariable Long id,
                              @RequestBody TaskDTO taskDTO) {
        taskDTO.setId(id);
        return taskFacade.createOrUpdateTask(taskDTO);
    }

    @PostMapping("/{id}/cases/import")
    public ResponseEntity<String> importCasesCSV(@PathVariable Long id,
                                            @RequestParam("file") MultipartFile file) {
        taskService.importCSV(id, file);
        return ResponseEntity.ok("CSV file successfully imported!");
    }

    @SneakyThrows
    @GetMapping("/{id}/cases/export")
    public void exportCasesCSV(@PathVariable Long id,
                               HttpServletResponse response) {
        response.setContentType(TEXT_CSV);
        response.addHeader(CONTENT_DISPOSITION_HEADER, CONTENT_DISPOSITION_VALUE);
        taskService.exportCasesCSV(id, response.getWriter());
    }


    @GetMapping("/search")
    public TaskPageResponse searchTaskByText(@RequestParam(defaultValue = "") String searchText,
                                             @RequestParam int page,
                                             @RequestParam int size) {
        return taskFacade.searchTaskByText(searchText, page, size);
    }
}
