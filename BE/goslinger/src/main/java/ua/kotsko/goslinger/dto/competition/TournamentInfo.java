package ua.kotsko.goslinger.dto.competition;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TournamentInfo {

    Long id;
    String title;
    String description;
    String startTime;
    String endTime;
    boolean isPublic;
    int participants;
    int maxParticipants;
    int tasks;


}
