package ua.kotsko.goslinger.utils;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.dto.tournament.TournamentDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;
import ua.kotsko.goslinger.dto.tournament.TournamentRowResponse;
import ua.kotsko.goslinger.entity.Tournament;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ConvertTournamentUtils extends ConvertUtils {

    public static TournamentDTO convertToTournamentDTO(Tournament tournament) {
        return TournamentDTO.builder()
                .id(tournament.getId())
                .title(tournament.getTitle())
                .description(tournament.getDescription())
                .startTime(tournament.getStartTime())
                .endTime(tournament.getEndTime())
                .isPublic(tournament.isPublic())
                .maxParticipants(tournament.getMaxParticipants())
                .password(tournament.getPassword())
                .build();
    }

    public static TournamentPageResponse convertToTournamentPage(Page<Tournament> tournamentPage) {
        return TournamentPageResponse.builder()
                .pageInfo(convertToPageInfo(tournamentPage))
                .tournamentRows(convertToTournamentRows(tournamentPage.getContent()))
                .build();
    }

    public static List<TournamentRowResponse> convertToTournamentRows(List<Tournament> content) {
        return content.stream()
                .map(ConvertTournamentUtils::convertToTournamentRow)
                .collect(Collectors.toList());
    }

    public static TournamentRowResponse convertToTournamentRow(Tournament tournament) {
        return TournamentRowResponse.builder()
                .id(tournament.getId())
                .title(tournament.getTitle())
                .isPublic(tournament.isPublic())
                .participants(tournament.getParticipants().size())
                .maxParticipants(tournament.getMaxParticipants())
                .amountTasks(tournament.getTasks().size())
                .startTime(convertToStringDate(tournament.getStartTime()))
                .endTime(convertToStringDate(tournament.getEndTime()))
                .build();
    }

}
