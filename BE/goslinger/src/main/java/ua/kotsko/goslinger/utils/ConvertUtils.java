package ua.kotsko.goslinger.utils;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.dto.PageResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertUtils {

    public static PageResponse convertToPageInfo(Page<?> page) {
        return PageResponse.builder()
                .page(page.getNumber())
                .size(page.getSize())
                .totalCount(page.getTotalElements())
                .totalPages(page.getTotalPages())
                .build();
    }

    protected static String convertToStringDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        return formatter.format(date);
    }

}
