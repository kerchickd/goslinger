package ua.kotsko.goslinger.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileDTO {

    private String firstname;
    private String lastname;
    private String email;
    private String icon;
    private Integer points;
    private Integer solvedTasks;
    private Integer tournamentParticipation;
    private Integer age;
    private String role;
    private Date birthdayDate;
    private boolean isEnabled;

}
