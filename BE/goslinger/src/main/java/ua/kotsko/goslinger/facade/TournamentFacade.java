package ua.kotsko.goslinger.facade;

import ua.kotsko.goslinger.dto.task.TaskList;
import ua.kotsko.goslinger.dto.tournament.TournamentDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;
import ua.kotsko.goslinger.dto.user.ParticipantList;

public interface TournamentFacade {
    TournamentDTO getTournamentById(Long id);

    TournamentDTO createOrUpdateTournament(TournamentDTO tournamentDTO);

    TournamentPageResponse searchTournamentByText(String searchText, int page, int size);

    TaskList getTournamentTasks(Long id);

    ParticipantList getTournamentParticipants(Long id);
}
