package ua.kotsko.goslinger.dto.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskDTO {

    private Long id;
    private String title;
    private Integer timeLimit;
    private Integer memoryLimit;
    private String problemStatement;
    private String inputText;
    private String outputText;
    private String exampleInputData;
    private String exampleOutputData;
    private String difficulty;

}
