package ua.kotsko.goslinger.service.implementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.repository.TournamentRepository;
import ua.kotsko.goslinger.service.CompetitionService;

@Service
@RequiredArgsConstructor
public class DefaultCompetitionService implements CompetitionService {

    private final TournamentRepository tournamentRepository;

    @Override
    public String tryToParticipate(User user, Tournament tournament, String password) {
        if (!tournament.isPublic() && !tournament.getPassword().equals(password)) {
            return "WRONG";
        }

        if (tournament.getParticipants().size() >= tournament.getMaxParticipants()) {
            return "MAX";
        }

        if (tournament.getParticipants().contains(user)) {
            return "ALREADY";
        }

        incrementUserTaskParticipation(user);
        tournament.getParticipants().add(user);
        tournamentRepository.save(tournament);
        return "SUCCESS";
    }

    @Override
    public Page<Tournament> searchTournamentAvailable(String searchText, int page, int size, User user) {
        searchText = searchText == null ? "" : searchText.strip();
        PageRequest pageRequest = PageRequest.of(page, size);
        return tournamentRepository.searchAvailableTournaments(searchText, user, pageRequest);
    }

    @Override
    public Page<Tournament> searchTournamentParticipated(String searchText, int page, int size, User user) {
        searchText = searchText == null ? "" : searchText.strip();
        PageRequest pageRequest = PageRequest.of(page, size);
        return tournamentRepository.searchParticipatedTournaments(searchText, user, pageRequest);
    }

    @Override
    public Page<Tournament> searchTournamentFinished(String searchText, int page, int size) {
        searchText = searchText == null ? "" : searchText.strip();
        PageRequest pageRequest = PageRequest.of(page, size);
        return tournamentRepository.searchFinishedTournaments(searchText, pageRequest);
    }

    private void incrementUserTaskParticipation(User user) {
        int tournamentParticipation = user.getInformation().getTournamentParticipation() + 1;
        user.getInformation().setTournamentParticipation(tournamentParticipation);
    }

}
