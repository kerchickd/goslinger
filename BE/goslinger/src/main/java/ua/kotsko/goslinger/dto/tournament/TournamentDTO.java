package ua.kotsko.goslinger.dto.tournament;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TournamentDTO {

    private Long id;
    private String title;
    private String description;
    private Date startTime;
    private Date endTime;
    private boolean isPublic;
    private String password;
    private int maxParticipants;

}
