package ua.kotsko.goslinger.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.kotsko.goslinger.entity.Submission;

@Repository
public interface SubmissionRepository extends JpaRepository<Submission, String> {
}
