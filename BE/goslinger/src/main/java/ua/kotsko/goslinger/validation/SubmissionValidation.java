package ua.kotsko.goslinger.validation;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.entity.Role;
import ua.kotsko.goslinger.entity.Submission;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.exception.ForbiddenAccessException;
import ua.kotsko.goslinger.service.SubmissionService;
import ua.kotsko.goslinger.service.UserService;

@Component
@RequiredArgsConstructor
public class SubmissionValidation {

    private final SubmissionService submissionService;
    private final UserService userService;

    @SneakyThrows
    public void validateAccess(String submissionId) {
        Submission submission = submissionService.getSubmission(submissionId);
        User currentUser = userService.getCurrentUser();

        if (!submission.getUser().equals(currentUser) && !currentUser.getRole().equals(Role.ADMIN)) {
            throw new ForbiddenAccessException("Access denied!");
        }
    }
}
