package ua.kotsko.goslinger.service;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.dto.user.UpdateCurrentUserRequest;
import ua.kotsko.goslinger.dto.user.UpdateUserRequest;
import ua.kotsko.goslinger.entity.Rating;
import ua.kotsko.goslinger.entity.User;

public interface UserService {

    User getCurrentUser();

    User updateUser(UpdateUserRequest updateUserRequest);

    User getUserById(Long id);

    User updateCurrentUser(UpdateCurrentUserRequest userRequest);

    Page<Rating> getUserRatingList(int page, int size);

    Page<User> searchUserByText(String searchText, int page, int size);
}
