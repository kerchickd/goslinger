package ua.kotsko.goslinger.utils;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.dto.task.TaskList;
import ua.kotsko.goslinger.dto.task.TaskPageResponse;
import ua.kotsko.goslinger.dto.task.TaskRowResponse;
import ua.kotsko.goslinger.entity.Task;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ConvertTaskUtils extends ConvertUtils{

    public static TaskDTO convertToTaskDTO(Task task) {
        return TaskDTO.builder()
                .id(task.getId())
                .title(task.getTitle())
                .memoryLimit(task.getMemoryLimit())
                .timeLimit(task.getTimeLimit())
                .problemStatement(task.getProblemStatement())
                .inputText(task.getInputText())
                .outputText(task.getOutputText())
                .difficulty(task.getDifficulty().getName())
                .exampleInputData(task.getExample().getInputData())
                .exampleOutputData(task.getExample().getOutputData())
                .build();
    }

    public static TaskPageResponse convertToTaskPageResponse(Page<Task> taskPage) {
        return TaskPageResponse.builder()
                .pageInfo(convertToPageInfo(taskPage))
                .taskRows(convertToTaskRows(taskPage.getContent()))
                .build();
    }

    public static List<TaskRowResponse> convertToTaskRows(List<Task> content) {
        return content.stream()
                .map(ConvertTaskUtils::convertToTaskRowResponse)
                .collect(Collectors.toList());
    }

    public static List<TaskRowResponse> convertToTaskRows(Set<Task> content) {
        return content.stream()
                .map(ConvertTaskUtils::convertToTaskRowResponse)
                .collect(Collectors.toList());
    }

    public static TaskRowResponse convertToTaskRowResponse(Task task) {
        return TaskRowResponse.builder()
                .id(task.getId())
                .title(task.getTitle())
                .memoryLimit(task.getMemoryLimit())
                .timeLimit(task.getTimeLimit())
                .testCasesAmount(task.getTestCaseList().size())
                .difficulty(task.getDifficulty().getName())
                .build();
    }

    public static TaskList convertToTaskList(Set<Task> tasks) {
        return TaskList.builder()
                .tasks(ConvertTaskUtils.convertToTaskRows(tasks))
                .build();
    }

}
