package ua.kotsko.goslinger.dto.compiler;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class TestCaseResult {

    String verdict;
    String statusResponse;
    int verdictStatusCode;
    String output;
    String error;
    String expectedOutput;
    int executionDuration;

}
