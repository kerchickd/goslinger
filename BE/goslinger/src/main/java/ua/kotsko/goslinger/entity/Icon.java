package ua.kotsko.goslinger.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
public enum Icon {

    FIRST("FIRST"),
    SECOND("SECOND"),
    THIRD("THIRD");

    @Getter
    private final String name;
    private static final List<Icon> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random(42);

    public static Icon randomIcon()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

}
