package ua.kotsko.goslinger.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse {

    private int page;
    private int size;
    private long totalCount;
    private int totalPages;
}
