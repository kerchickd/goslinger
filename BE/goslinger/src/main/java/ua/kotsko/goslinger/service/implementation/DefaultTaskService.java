package ua.kotsko.goslinger.service.implementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.entity.Difficulty;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.entity.TestCase;
import ua.kotsko.goslinger.repository.TaskRepository;
import ua.kotsko.goslinger.repository.TestCaseRepository;
import ua.kotsko.goslinger.service.TaskService;

import java.io.*;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DefaultTaskService implements TaskService {

    private static final String HEADER_CSV_INPUT_DATA = "input_data";
    private static final String HEADER_CSV_OUTPUT_DATA = "output_data";
    private final TaskRepository taskRepository;
    private final TestCaseRepository testCaseRepository;
    private final CsvFileGenerator csvFileGenerator;

    @Override
    @Transactional
    public Task createOrUpdateTask(TaskDTO taskDTO) {
        Task task;
        if (taskDTO.getId() != null) {
            task = updateTask(this.getTaskById(taskDTO.getId()), taskDTO);
        } else {
            task = createTask(taskDTO);
        }
        task = taskRepository.save(task);
        return task;
    }

    private static Task createTask(TaskDTO taskDTO) {
        Task task = Task.builder()
                .title(taskDTO.getTitle())
                .memoryLimit(taskDTO.getMemoryLimit())
                .timeLimit(taskDTO.getTimeLimit())
                .problemStatement(taskDTO.getProblemStatement())
                .inputText(taskDTO.getInputText())
                .outputText(taskDTO.getOutputText())
                .difficulty(Difficulty.valueOf(taskDTO.getDifficulty()))
                .build();
        task.addExample(taskDTO.getExampleInputData(), taskDTO.getExampleOutputData());
        return task;
    }

    private Task updateTask(Task task, TaskDTO taskDTO) {
        task.setTitle(taskDTO.getTitle());
        task.setMemoryLimit(taskDTO.getMemoryLimit());
        task.setTimeLimit(taskDTO.getTimeLimit());
        task.setProblemStatement(taskDTO.getProblemStatement());
        task.setInputText(taskDTO.getInputText());
        task.setOutputText(taskDTO.getOutputText());
        task.setDifficulty(Difficulty.valueOf(taskDTO.getDifficulty()));
        task.getExample().setInputData(taskDTO.getExampleInputData());
        task.getExample().setOutputData(taskDTO.getExampleOutputData());
        return task;
    }

    @Override
    public Task getTaskById(Long id) {
        return taskRepository.getReferenceById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void importCSV(Long id, MultipartFile file) {
        Task task = this.getTaskById(id);
        if (task.getTestCaseList() != null) {
            testCaseRepository.deleteAll(task.getTestCaseList());
            task.getTestCaseList().clear();
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader());
            for (CSVRecord record : csvParser) {
                task.addTestCase(record.get(HEADER_CSV_INPUT_DATA), record.get(HEADER_CSV_OUTPUT_DATA));
            }
        }
        testCaseRepository.saveAll(task.getTestCaseList());
    }

    @Override
    public void exportCasesCSV(Long id, Writer writer) {
        Task task = this.getTaskById(id);
        List<TestCase> testCases = task.getTestCaseList();
        csvFileGenerator.writeTestCasesToCSV(testCases, writer);
    }

    @Override
    public Page<Task> searchTaskByText(String searchText, int page, int size) {
        searchText = searchText == null ? "" : searchText.strip();
        PageRequest pageRequest = PageRequest.of(page, size);
        return taskRepository.searchPagingTasks(searchText, pageRequest);
    }

}
