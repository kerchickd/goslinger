package ua.kotsko.goslinger.dto.competition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BodyResult {

    private Long id;
    private String icon;
    private String firstname;
    private String lastname;
    private int totalMark;
    private List<Integer> marks;

}
