package ua.kotsko.goslinger.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kotsko.goslinger.entity.Rating;
import ua.kotsko.goslinger.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    @Query(value = "SELECT new ua.kotsko.goslinger.entity.Rating(ROW_NUMBER() OVER (ORDER BY ui.points DESC), u) " +
            "FROM User u JOIN UserInformation ui ON u.id = ui.user.id " +
            "WHERE u.isEnabled = true " +
            "ORDER BY ROW_NUMBER() OVER (ORDER BY ui.points DESC)",
            countQuery = "SELECT COUNT(u) FROM User u WHERE u.isEnabled = true ")
    Page<Rating> findRatingOfUserByPoints(Pageable pageable);

    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.firstname LIKE %:searchText% " +
            "OR u.lastname LIKE %:searchText% " +
            "OR u.email LIKE %:searchText% " +
            "ORDER BY u.id",
            countQuery = "SELECT COUNT(u) " +
                    "FROM User u " +
                    "WHERE u.firstname LIKE %:searchText% " +
                    "OR u.lastname LIKE %:searchText% " +
                    "OR u.email LIKE %:searchText% ")
    Page<User> searchPagingUsers(@Param("searchText") String searchText, Pageable pageable);


}
