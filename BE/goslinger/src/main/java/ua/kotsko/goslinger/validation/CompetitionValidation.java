package ua.kotsko.goslinger.validation;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.exception.ForbiddenAccessException;
import ua.kotsko.goslinger.exception.TaskNotContainInTournament;
import ua.kotsko.goslinger.service.TaskService;
import ua.kotsko.goslinger.service.TournamentService;
import ua.kotsko.goslinger.service.UserService;

import java.util.Date;

@Component
@RequiredArgsConstructor
public class CompetitionValidation {

    private final UserService userService;
    private final TaskService taskService;
    private final TournamentService tournamentService;

    @SneakyThrows
    public void validateAccess(Long tournamentId) {
        User user = userService.getCurrentUser();
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        Date currentDate = new Date(System.currentTimeMillis());
        if (tournament.getEndTime().before(currentDate)) {
            return;
        }
        if (!tournament.getParticipants().contains(user)
                || tournament.getStartTime().after(currentDate)) {
            throw new ForbiddenAccessException("Access denied!");
        }
    }

    @SneakyThrows
    public void validateSendRequest(Long tournamentId, Long taskId) {
        Tournament tournament = tournamentService.getTournamentById(tournamentId);
        Task task = taskService.getTaskById(taskId);
        User currentUser = userService.getCurrentUser();
        if (!tournament.getTasks().contains(task)) {
            throw new TaskNotContainInTournament("Wrong task!");
        }
        Date currentTime = new Date(System.currentTimeMillis());
        if (!tournament.getParticipants().contains(currentUser)
                || tournament.getStartTime().after(currentTime)
                || tournament.getEndTime().before(currentTime)) {
            throw new ForbiddenAccessException("Access denied!");
        }
    }

}
