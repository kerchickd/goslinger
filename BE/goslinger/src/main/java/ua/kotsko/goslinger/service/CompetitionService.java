package ua.kotsko.goslinger.service;

import org.springframework.data.domain.Page;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;

public interface CompetitionService {
    String tryToParticipate(User user, Tournament tournament, String password);

    Page<Tournament> searchTournamentAvailable(String searchText, int page, int size, User user);

    Page<Tournament> searchTournamentParticipated(String searchText, int page, int size, User user);

    Page<Tournament> searchTournamentFinished(String searchText, int page, int size);
}
