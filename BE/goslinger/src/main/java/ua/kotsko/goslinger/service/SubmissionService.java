package ua.kotsko.goslinger.service;

import ua.kotsko.goslinger.dto.compiler.CodeDTO;
import ua.kotsko.goslinger.dto.compiler.CodeRequest;
import ua.kotsko.goslinger.dto.compiler.CodeResponse;
import ua.kotsko.goslinger.entity.Submission;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;

import java.util.List;
import java.util.Set;

public interface SubmissionService {

    Submission getSubmission(String id);

    int getMaxMark(Task task, User user, Set<Submission> submissions);

    List<String> generateAlphabet(int size);

    Submission createSubmission(Task task, Tournament tournament, CodeDTO codeDTO);

    void saveResult(Submission submission, CodeResponse response, int maxMark);
}
