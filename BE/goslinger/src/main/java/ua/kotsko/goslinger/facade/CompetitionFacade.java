package ua.kotsko.goslinger.facade;

import ua.kotsko.goslinger.dto.competition.PasswordRequest;
import ua.kotsko.goslinger.dto.competition.TournamentInfo;
import ua.kotsko.goslinger.dto.competition.TournamentResults;
import ua.kotsko.goslinger.dto.competition.TournamentTasks;
import ua.kotsko.goslinger.dto.compiler.CodeDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;

public interface CompetitionFacade {

    String participate(Long tournamentId, PasswordRequest passwordRequest);

    TournamentPageResponse searchTournamentAvailable(String searchText, int page, int size);

    TournamentPageResponse searchTournamentParticipated(String searchText, int page, int size);

    TournamentPageResponse searchTournamentFinished(String searchText, int page, int size);

    TournamentInfo getTournamentInfo(Long id);

    TournamentTasks getTournamentTasks(Long id);

    TournamentResults getTournamentResults(Long id);

    SubmissionDTO createAndSendRequestToCompiler(Long tournamentId, Long taskId, CodeDTO codeDTO);
}
