package ua.kotsko.goslinger.dto.compiler;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class TestCaseDTO {

    String input;
    String expectedOutput;

}
