package ua.kotsko.goslinger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.kotsko.goslinger.dto.compiler.CodeResponse;
import ua.kotsko.goslinger.dto.submission.SubmissionDTO;
import ua.kotsko.goslinger.dto.submission.SubmissionList;
import ua.kotsko.goslinger.facade.SubmissionFacade;
import ua.kotsko.goslinger.validation.SubmissionValidation;

@RestController
@RequestMapping("/api/submission")
@RequiredArgsConstructor
public class SubmissionController {

    private final SubmissionFacade submissionFacade;
    private final SubmissionValidation submissionValidation;

    @GetMapping("/current/user")
    public SubmissionList getCurrentUserSubmissions() {
        return submissionFacade.getAllCurrentUserSubmissions();
    }

    @GetMapping("/all/view/{userId}/user")
    public SubmissionList getUserSubmissions(@PathVariable Long userId) {
        return submissionFacade.getAllUserSubmissions(userId);
    }

    @GetMapping("/get/{submissionId}")
    public SubmissionDTO getSubmission(@PathVariable String submissionId) {
        submissionValidation.validateAccess(submissionId);
        return submissionFacade.getSubmission(submissionId);
    }

}
