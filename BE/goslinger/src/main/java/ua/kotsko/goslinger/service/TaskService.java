package ua.kotsko.goslinger.service;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.entity.Task;

import java.io.Writer;

public interface TaskService {

    Task createOrUpdateTask(TaskDTO taskDTO);

    Task getTaskById(Long id);

    void importCSV(Long id, MultipartFile file);

    void exportCasesCSV(Long id, Writer writer);

    Page<Task> searchTaskByText(String searchText, int page, int size);

}
