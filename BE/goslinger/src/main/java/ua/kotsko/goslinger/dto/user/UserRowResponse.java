package ua.kotsko.goslinger.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRowResponse {

    private long id;
    private String firstname;
    private String lastname;
    private String icon;
    private int age;
    private String email;
    private boolean isEnabled;
    private String role;

}
