package ua.kotsko.goslinger.dto.compiler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.kotsko.goslinger.entity.Language;

import java.util.LinkedHashMap;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CodeRequest {

    String sourcecode;
    Language language;
    int timeLimit;
    int memoryLimit;
    LinkedHashMap<String, TestCaseDTO> testCases;

}
