package ua.kotsko.goslinger.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicInsert
@ToString(exclude = "user")
@Table(name = "users_information")
public class UserInformation {

    @Id
    private Long id;

    @ColumnDefault("0")
    private Integer points;

    @ColumnDefault("0")
    private Integer solvedTasks;

    @ColumnDefault("0")
    private Integer tournamentParticipation;

    private Date birthayDate;

    @OneToOne
    @MapsId
    private User user;

}
