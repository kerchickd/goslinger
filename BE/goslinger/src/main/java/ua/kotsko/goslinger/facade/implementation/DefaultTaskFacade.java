package ua.kotsko.goslinger.facade.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.dto.task.TaskDTO;
import ua.kotsko.goslinger.dto.task.TaskPageResponse;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.facade.TaskFacade;
import ua.kotsko.goslinger.service.TaskService;
import ua.kotsko.goslinger.utils.ConvertTaskUtils;

@Component
@RequiredArgsConstructor
public class DefaultTaskFacade implements TaskFacade {

    private final TaskService taskService;

    @Override
    public TaskDTO createOrUpdateTask(TaskDTO taskDTO) {
        Task task = taskService.createOrUpdateTask(taskDTO);
        return ConvertTaskUtils.convertToTaskDTO(task);
    }

    @Override
    public TaskDTO getTaskById(Long id) {
        Task task = taskService.getTaskById(id);
        return ConvertTaskUtils.convertToTaskDTO(task);
    }

    @Override
    public TaskPageResponse searchTaskByText(String searchText, int page, int size) {
        Page<Task> taskPage = taskService.searchTaskByText(searchText, page, size);
        return ConvertTaskUtils.convertToTaskPageResponse(taskPage);
    }
}
