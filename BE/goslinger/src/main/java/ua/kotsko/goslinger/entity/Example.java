package ua.kotsko.goslinger.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicInsert
@ToString(exclude = {"task"})
@Table(name = "examples")
public class Example {

    @Id
    private Long id;
    private String inputData;
    private String outputData;
    @OneToOne
    @MapsId
    private Task task;
}
