package ua.kotsko.goslinger.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.kotsko.goslinger.entity.TestCase;

@Repository
public interface TestCaseRepository extends JpaRepository<TestCase, String> {
}
