package ua.kotsko.goslinger.service.implementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ua.kotsko.goslinger.dto.tournament.TournamentDTO;
import ua.kotsko.goslinger.entity.Task;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.repository.TournamentRepository;
import ua.kotsko.goslinger.repository.UserRepository;
import ua.kotsko.goslinger.service.TaskService;
import ua.kotsko.goslinger.service.TournamentService;
import ua.kotsko.goslinger.service.UserService;

@Service
@RequiredArgsConstructor
public class DefaultTournamentService implements TournamentService {

    private final TournamentRepository tournamentRepository;
    private final TaskService taskService;
    private final UserService userService;

    @Override
    public Tournament getTournamentById(Long id) {
        return tournamentRepository.getReferenceById(id);
    }

    @Override
    public Tournament createOrUpdateTournament(TournamentDTO tournamentDTO) {
        Tournament tournament;
        if (tournamentDTO.getId() != null) {
            tournament = updateTournament(this.getTournamentById(tournamentDTO.getId()), tournamentDTO);
        } else {
            tournament = createTournament(tournamentDTO);
        }
        tournament = tournamentRepository.save(tournament);
        return tournament;
    }

    @Override
    public Page<Tournament> searchTournamentByText(String searchText, int page, int size) {
        searchText = searchText == null ? "" : searchText.strip();
        PageRequest pageRequest = PageRequest.of(page, size);
        return tournamentRepository.searchPagingTournaments(searchText, pageRequest);
    }

    @Override
    @Transactional
    public void addTask(Long id, Long taskId) {
        Tournament tournament = this.getTournamentById(id);
        Task task = taskService.getTaskById(taskId);
        tournament.getTasks().add(task);
        tournamentRepository.save(tournament);
    }

    @Override
    @Transactional
    public void removeTask(Long id, Long taskId) {
        Tournament tournament = this.getTournamentById(id);
        Task task = taskService.getTaskById(taskId);
        removeAllTaskTournamentConnections(tournament, task);
        tournamentRepository.save(tournament);
    }

    private void removeAllTaskTournamentConnections(Tournament tournament, Task task) {
        tournament.getTasks().remove(task);
        tournament.getSubmissions().removeIf(submission -> submission.getTask().equals(task));
    }

    @Override
    @Transactional
    public void removeParticipant(Long id, Long participantId) {
        Tournament tournament = this.getTournamentById(id);
        User user = userService.getUserById(participantId);
        int tournamentParticipation = user.getInformation().getTournamentParticipation() - 1;
        user.getInformation().setTournamentParticipation(tournamentParticipation);
        removeAllParticipantTournamentConnections(tournament, user);
        tournamentRepository.save(tournament);
    }

    private void removeAllParticipantTournamentConnections(Tournament tournament, User user) {
        tournament.getParticipants().remove(user);
        tournament.getSubmissions().removeIf(submission -> submission.getUser().equals(user));
    }

    private Tournament updateTournament(Tournament tournament, TournamentDTO tournamentDTO) {
        tournament.setTitle(tournamentDTO.getTitle());
        tournament.setDescription(tournamentDTO.getDescription());
        tournament.setStartTime(tournamentDTO.getStartTime());
        tournament.setEndTime(tournamentDTO.getEndTime());
        tournament.setPassword(
                tournamentDTO.isPublic() ?
                StringUtils.EMPTY :
                tournamentDTO.getPassword()
        );
        tournament.setPublic(tournamentDTO.isPublic());
        tournament.setMaxParticipants(tournamentDTO.getMaxParticipants());
        return tournament;
    }

    private Tournament createTournament(TournamentDTO tournamentDTO) {
        return Tournament.builder()
                .title(tournamentDTO.getTitle())
                .description(tournamentDTO.getDescription())
                .startTime(tournamentDTO.getStartTime())
                .endTime(tournamentDTO.getEndTime())
                .isPublic(tournamentDTO.isPublic())
                .maxParticipants(tournamentDTO.getMaxParticipants())
                .password(
                        tournamentDTO.isPublic() ?
                        StringUtils.EMPTY :
                        tournamentDTO.getPassword()
                )
                .build();
    }

}
