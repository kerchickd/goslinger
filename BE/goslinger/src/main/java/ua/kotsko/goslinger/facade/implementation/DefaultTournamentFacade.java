package ua.kotsko.goslinger.facade.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.dto.task.TaskList;
import ua.kotsko.goslinger.dto.tournament.TournamentDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;
import ua.kotsko.goslinger.dto.user.ParticipantList;
import ua.kotsko.goslinger.entity.Tournament;
import ua.kotsko.goslinger.facade.TournamentFacade;
import ua.kotsko.goslinger.service.TournamentService;
import ua.kotsko.goslinger.utils.ConvertTaskUtils;
import ua.kotsko.goslinger.utils.ConvertTournamentUtils;
import ua.kotsko.goslinger.utils.ConvertUserUtils;

@Component
@RequiredArgsConstructor
public class DefaultTournamentFacade implements TournamentFacade {

    private final TournamentService tournamentService;

    @Override
    public TournamentDTO getTournamentById(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        return ConvertTournamentUtils.convertToTournamentDTO(tournament);
    }

    @Override
    public TournamentDTO createOrUpdateTournament(TournamentDTO tournamentDTO) {
        Tournament tournament = tournamentService.createOrUpdateTournament(tournamentDTO);
        return ConvertTournamentUtils.convertToTournamentDTO(tournament);
    }

    @Override
    public TournamentPageResponse searchTournamentByText(String searchText,
                                                         int page,
                                                         int size) {
        Page<Tournament> tournamentPage = tournamentService.searchTournamentByText(searchText, page, size);
        return ConvertTournamentUtils.convertToTournamentPage(tournamentPage);
    }

    @Override
    public TaskList getTournamentTasks(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        return ConvertTaskUtils.convertToTaskList(tournament.getTasks());
    }

    @Override
    public ParticipantList getTournamentParticipants(Long id) {
        Tournament tournament = tournamentService.getTournamentById(id);
        return ConvertUserUtils.convertToParticipantList(tournament.getParticipants());
    }

}
