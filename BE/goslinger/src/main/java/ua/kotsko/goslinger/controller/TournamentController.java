package ua.kotsko.goslinger.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ua.kotsko.goslinger.dto.task.TaskList;
import ua.kotsko.goslinger.dto.tournament.TournamentDTO;
import ua.kotsko.goslinger.dto.tournament.TournamentPageResponse;
import ua.kotsko.goslinger.dto.user.ParticipantList;
import ua.kotsko.goslinger.facade.TournamentFacade;
import ua.kotsko.goslinger.service.TournamentService;
import ua.kotsko.goslinger.validation.TournamentValidation;

@RestController
@RequestMapping("/api/tournament")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequiredArgsConstructor
public class TournamentController {

    private final TournamentService tournamentService;
    private final TournamentFacade tournamentFacade;
    private final TournamentValidation tournamentValidation;

    @GetMapping("/{id}")
    public TournamentDTO getTournamentById(@PathVariable Long id){
        return tournamentFacade.getTournamentById(id);
    }

    @PostMapping("/create")
    public TournamentDTO createTournament(@RequestBody TournamentDTO tournamentDTO) {
        return tournamentFacade.createOrUpdateTournament(tournamentDTO);
    }

    @PutMapping("/{id}/update")
    public TournamentDTO updateTournament(@PathVariable Long id,
                                          @RequestBody TournamentDTO tournamentDTO) {
        tournamentValidation.validateTime(id);
        tournamentDTO.setId(id);
        return tournamentFacade.createOrUpdateTournament(tournamentDTO);
    }

    @PostMapping("/{id}/add/task/{taskId}")
    public void addTask(@PathVariable Long id,
                        @PathVariable Long taskId) {
        tournamentValidation.validateTime(id);
        tournamentService.addTask(id, taskId);
    }

    @DeleteMapping("/{id}/remove/task/{taskId}")
    public void removeTask(@PathVariable Long id,
                        @PathVariable Long taskId) {
        tournamentValidation.validateTime(id);
        tournamentService.removeTask(id, taskId);
    }

    @DeleteMapping("/{id}/remove/participant/{participantId}")
    public void removeParticipant(@PathVariable Long id,
                           @PathVariable Long participantId) {
        tournamentValidation.validateTime(id);
        tournamentService.removeParticipant(id, participantId);
    }

    @GetMapping("/search")
    public TournamentPageResponse searchTournamentByText(@RequestParam(defaultValue = "") String searchText,
                                                         @RequestParam int page,
                                                         @RequestParam int size) {
        return tournamentFacade.searchTournamentByText(searchText, page, size);
    }

    @GetMapping("/{id}/tasks")
    public TaskList getTournamentTasks(@PathVariable Long id) {
        return tournamentFacade.getTournamentTasks(id);
    }

    @GetMapping("/{id}/participants")
    public ParticipantList getTournamentParticipants(@PathVariable Long id) {
        return tournamentFacade.getTournamentParticipants(id);
    }

}
