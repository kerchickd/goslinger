package ua.kotsko.goslinger.facade.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.dto.user.*;
import ua.kotsko.goslinger.entity.Rating;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.facade.UserFacade;
import ua.kotsko.goslinger.service.UserService;
import ua.kotsko.goslinger.utils.ConvertUserUtils;

@Component
@RequiredArgsConstructor
public class DefaultUserFacade implements UserFacade {

    private final UserService userService;

    @Override
    public ProfileDTO getCurrentUserProfile() {
        User user = userService.getCurrentUser();
        return ConvertUserUtils.convertToProfileDTO(user);
    }

    @Override
    public ProfileDTO getUserProfile(Long id) {
        User user = userService.getUserById(id);
        return ConvertUserUtils.convertToProfileDTO(user);
    }

    @Override
    public ProfileDTO updateUserFull(UpdateUserRequest updateUserRequest) {
        User user = userService.updateUser(updateUserRequest);
        return ConvertUserUtils.convertToProfileDTO(user);
    }

    @Override
    public ProfileDTO updateCurrentUser(UpdateCurrentUserRequest userRequest) {
        User user = userService.updateCurrentUser(userRequest);
        return ConvertUserUtils.convertToProfileDTO(user);
    }

    @Override
    public RatingResponse getUserRatingList(int page, int size) {
        Page<Rating> ratingPage = userService.getUserRatingList(page, size);
        return ConvertUserUtils.convertToRatingResponse(ratingPage);
    }

    @Override
    public UserPageResponse searchUserByText(String searchText, int page, int size) {
        Page<User> userPage = userService.searchUserByText(searchText, page, size);
        return ConvertUserUtils.convertToUserPageResponse(userPage);
    }

}
