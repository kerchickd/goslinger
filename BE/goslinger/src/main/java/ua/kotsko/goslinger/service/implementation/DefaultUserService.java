package ua.kotsko.goslinger.service.implementation;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ua.kotsko.goslinger.dto.user.UpdateCurrentUserRequest;
import ua.kotsko.goslinger.dto.user.UpdateUserRequest;
import ua.kotsko.goslinger.entity.Rating;
import ua.kotsko.goslinger.entity.Role;
import ua.kotsko.goslinger.entity.User;
import ua.kotsko.goslinger.exception.UserNotFoundException;
import ua.kotsko.goslinger.repository.UserRepository;
import ua.kotsko.goslinger.service.UserService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;

    @Override
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        return principal instanceof User ? this.getUserById(((User) principal).getId()) : null;
    }

    @SneakyThrows
    @Override
    public User updateUser(UpdateUserRequest updateUserRequest) {
        User user;
        Optional<User> optionalUser = userRepository.findById(updateUserRequest.getId());
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        } else {
            throw new UserNotFoundException(String.format("User with %d id not found.", updateUserRequest.getId()));
        }
        user.setFirstname(updateUserRequest.getFirstname());
        user.setLastname(updateUserRequest.getLastname());
        user.getInformation().setBirthayDate(updateUserRequest.getBirthdayDate());
        if (updateUserRequest.isChangeRoleToAdmin()) {
            user.setRole(Role.ADMIN);
        }
        user.setEnabled(updateUserRequest.isEnabled());
        user = userRepository.save(user);
        return user;
    }

    @SneakyThrows
    @Override
    public User getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UserNotFoundException(String.format("User with %d id not found.", id));
        }
    }

    @Override
    public User updateCurrentUser(UpdateCurrentUserRequest userRequest) {
        User user = this.getCurrentUser();
        user.setFirstname(userRequest.getFirstname());
        user.setLastname(userRequest.getLastname());
        user.getInformation().setBirthayDate(userRequest.getBirthdayDate());
        user = userRepository.save(user);
        return user;
    }

    @Override
    public Page<Rating> getUserRatingList(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        return userRepository.findRatingOfUserByPoints(pageRequest);
    }

    @Override
    public Page<User> searchUserByText(String searchText, int page, int size) {
        searchText = searchText == null ? "" : searchText.strip();
        PageRequest pageRequest = PageRequest.of(page, size);
        return userRepository.searchPagingUsers(searchText, pageRequest);
    }

}
