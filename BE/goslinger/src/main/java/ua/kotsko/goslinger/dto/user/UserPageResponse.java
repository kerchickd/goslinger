package ua.kotsko.goslinger.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.kotsko.goslinger.dto.PageResponse;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPageResponse {

    private PageResponse pageInfo;
    private List<UserRowResponse> userRows;

}
