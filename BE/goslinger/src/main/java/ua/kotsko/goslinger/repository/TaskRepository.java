package ua.kotsko.goslinger.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kotsko.goslinger.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query(value = "SELECT t " +
            "FROM Task t " +
            "WHERE t.title LIKE %:searchText% " +
            "OR t.problemStatement LIKE %:searchText% " +
            "ORDER BY t.id",
            countQuery = "SELECT COUNT(t) " +
                    "FROM Task t " +
                    "WHERE t.title LIKE %:searchText% " +
                    "OR t.problemStatement LIKE %:searchText% ")
    Page<Task> searchPagingTasks(@Param("searchText") String searchText, Pageable pageable);

}
