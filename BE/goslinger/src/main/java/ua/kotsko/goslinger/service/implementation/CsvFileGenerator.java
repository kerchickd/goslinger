package ua.kotsko.goslinger.service.implementation;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;
import ua.kotsko.goslinger.entity.TestCase;

import java.io.Writer;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CsvFileGenerator {

    private static final String HEADER_CSV_INPUT_DATA = "input_data";
    private static final String HEADER_CSV_OUTPUT_DATA = "output_data";

    @SneakyThrows
    public void writeTestCasesToCSV(List<TestCase> testCases,
                                    Writer writer) {
        CSVFormat csvFormat = CSVFormat.Builder
                .create()
                .setHeader(HEADER_CSV_INPUT_DATA, HEADER_CSV_OUTPUT_DATA)
                .build();
        CSVPrinter csvPrinter = new CSVPrinter(writer, csvFormat);

        for (TestCase testCase : testCases) {
            csvPrinter.printRecord(testCase.getInputData(), testCase.getOutputData());
        }

        csvPrinter.flush();
        csvPrinter.close();
    }

}
