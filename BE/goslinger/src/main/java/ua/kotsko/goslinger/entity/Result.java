package ua.kotsko.goslinger.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString(exclude = {"submission"})
@DynamicInsert
@Table(name = "results")
public class Result {

    @Id
    private String id;

    private int mark;

    private int status;

    @Column(columnDefinition = "TEXT")
    private String error;

    private String verdict;

    private int compilationDuration;

    private float averageExecutionDuration;

    @OneToOne
    @MapsId
    private Submission submission;

}
