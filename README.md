GOSLINGER platform

Goslinger is an online platform that provides a variety of resources and services related to competitive programming and algorithmic problem-solving. It is primarily focused on organizing and hosting programming contests for individuals interested in improving their coding skills. Supported 12 programming languages: Java, Kotlin, C, C++, C#, Golang, Python, Scala, Ruby, Rust, Haskell and Swift.

For checking users submission, we use microservice RemoteCodeCompiler, basically constructed by Zakaria Maaraki. Adding support for 2 languages: Swift, Ruby.

The external appearance of platform:

![Auth page](/uploads/5ed207d3134e148d115176de7d646b95/image.png)
![Home page](/uploads/7b6f06d9affd24f7fe45c5a12dd9f8e0/homepage.PNG)
![Profile page](/uploads/277c2fd2500ee572c8d27180587b4cae/image.png)
![Task management list](/uploads/1ea8d5097e875d582f801cff67b3669f/image.png)
![Edit task page](/uploads/be892fbc7f35c50d0fe01b05fdc3d65c/image.png)
![Edit tournament page](/uploads/25996902c1d12b84e3f2b5e8ab413415/image.png)
![Rating page](/uploads/9b6bddcc6cf34ac50529c67039559a0c/image.png)
![Tournaments page](/uploads/0df3c5b959c10b104142d4cac5ab0acc/image.png)
![Tournament info](/uploads/50f2d0ae682dbcde0337b19cf0c55cd4/image.png)
![Tournament result table](/uploads/8871970caef0b03f169327fa87987c8a/image.png)
![Send submission page](/uploads/d08703672f31f6e3787fc84c4cfcd3b7/image.png)
![Submissions](/uploads/58b06437e8af91af9efb34a856ba9a87/image.png)
![Submission](/uploads/cdd8a6a3ebdac615d25dcb65c058798c/image.png)
