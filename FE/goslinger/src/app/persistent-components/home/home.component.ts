import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserAuthService} from "../../core/services/user-auth.service";
import {UserService} from "../../core/services/user.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,
              private userService: UserService,
              private userAuthService: UserAuthService) {
  }

  ngOnInit(): void {
  }

}
