import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../core/services/user.service";
import {UserAuthService} from "../../core/services/user-auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public router : Router,
              public userService : UserService,
              public userAuthService : UserAuthService) { }

  ngOnInit(): void {
  }
  public isLoggedIn() : boolean {
    return this.userAuthService.isLoggedIn();
  }

  logout() : void {
    this.userService.logout();
    this.userAuthService.clear();
    this.router.navigate(['/login']);
  }

}
