import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PasswordRequest, TournamentPageResponse} from "../models/tournament.models";
import {TaskDTO, TaskPageResponse} from "../models/task.models";
import {CodeDTO, SubmissionDTO} from "../models/submission.models";

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

  constructor(
    private httpClient: HttpClient
  ) { }

  participate(id: number, body: PasswordRequest) {
    return this.httpClient.post(`/competition/${id}/participate`, body, {
      responseType: 'text'
    });
  }

  searchTournamentAvailable(searchText: string, page: number, size: number): Observable<TournamentPageResponse> {
    const params = new HttpParams()
      .set('searchText', searchText)
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<TournamentPageResponse>('/competition/available/search', {params});
  }

  searchTournamentFinished(searchText: string, page: number, size: number): Observable<TournamentPageResponse> {
    const params = new HttpParams()
      .set('searchText', searchText)
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<TournamentPageResponse>('/competition/finished/search', {params});
  }

  searchTournamentParticipated(searchText: string, page: number, size: number): Observable<TournamentPageResponse> {
    const params = new HttpParams()
      .set('searchText', searchText)
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<TournamentPageResponse>('/competition/participated/search', {params});
  }

  createAndSendSubmission(tournamentId: number, taskId: number, body: CodeDTO) : Observable<SubmissionDTO> {
    return this.httpClient.post<SubmissionDTO>(
      `/competition/tournament/${tournamentId}/task/${taskId}/submission/create`, body, {
        responseType: 'json'
    });
  }

  getTaskDTO(tournamentId: number, taskId: number) : Observable<TaskDTO> {
    return this.httpClient.get<TaskDTO>(`/competition/tournament/${tournamentId}/task/${taskId}`, {
      responseType: 'json'
    });
  }

}
