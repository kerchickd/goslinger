import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TournamentDTO} from "../models/tournament.models";

@Injectable({
  providedIn: 'root'
})
export class TournamentService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getTournamentById(id: number) : Observable<TournamentDTO> {
    return this.httpClient.get<TournamentDTO>(`/tournament/${id}`, {
      responseType: 'json',
    });
  }

  createTournament(body: TournamentDTO) : Observable<TournamentDTO> {
    return this.httpClient.post<TournamentDTO>('/tournament/create', body, {
      responseType: 'json',
    });
  }

  updateTournament(id: number, body: TournamentDTO) : Observable<TournamentDTO> {
    return this.httpClient.put<TournamentDTO>(`/tournament/${id}/update`, body, {
      responseType: 'json',
    });
  }

  addTask(id: number, taskId: number): Observable<void> {
    return this.httpClient.post<void>(`/tournament/${id}/add/task/${taskId}`, null);
  }

  removeTask(id: number, taskId: number): Observable<void> {
    return this.httpClient.delete<void>(`/tournament/${id}/remove/task/${taskId}`);
  }

  removeParticipant(id: number, participantId: number): Observable<void> {
    return this.httpClient.delete<void>(`/tournament/${id}/remove/participant/${participantId}`);
  }

}
