import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {RatingResponse} from "../models/rating.models";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getRatingPageList(page: number, size: number): Observable<RatingResponse> {
    const params = new HttpParams()
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<RatingResponse>('/user/rating', { params });
  }

}
