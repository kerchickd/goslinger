import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {
  AuthenticationRequest,
  ProfileDTO,
  RegisterRequest, UpdateCurrentUserRequest, UpdateUserRequest
} from "../models/user.models";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private httpClient: HttpClient
  ) {
  }

  public login(loginData: AuthenticationRequest) {
    return this.httpClient.post('/auth/authenticate', loginData, {
      responseType: 'json',
    });
  }

  public register(registerData: RegisterRequest) {
    return this.httpClient.post('/auth/register', registerData, {
      responseType: 'json',
    });
  }

  public logout(): void {
    this.httpClient.get('/auth/logout').subscribe();
  }

  public getCurrentUserProfile() : Observable<ProfileDTO> {
    return this.httpClient.get<ProfileDTO>('/user/current/profile', {
      responseType: 'json',
    });
  }

  public updateCurrentUserProfile(body: UpdateCurrentUserRequest) : Observable<ProfileDTO> {
    return this.httpClient.put<ProfileDTO>('/user/current/profile/update', body, {
      responseType: 'json',
    });
  }

}
