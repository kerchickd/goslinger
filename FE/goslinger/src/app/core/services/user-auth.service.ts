import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class UserAuthService {
  constructor() {}

  public setEmail(email: string) {
    localStorage.setItem('email', email);
  }

  public getEmail(): string | null {
    return localStorage.getItem('email');
  }

  public setRole(role: string) {
    localStorage.setItem('role', role);
  }

  public getRole(): string | null {
    return localStorage.getItem('role');
  }

  public setToken(jwtToken: string) {
    localStorage.setItem('jwtToken', jwtToken);
  }

  public getToken(): string | null {
    return localStorage.getItem('jwtToken');
  }

  public clear() {
    localStorage.clear();
  }

  public isLoggedIn(): boolean {
    return this.getRole() !== null && this.getToken() !== null;
  }
}
