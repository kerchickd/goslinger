import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {SubmissionDTO, SubmissionList} from "../models/submission.models";

@Injectable({
  providedIn: 'root'
})
export class SubmissionService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getCurrentUserSubmissions() : Observable<SubmissionList> {
    return this.httpClient.get<SubmissionList>('/submission/current/user', {
      responseType: 'json'
    });
  }

  getUserSubmissions(userId: number) : Observable<SubmissionList> {
    return this.httpClient.get<SubmissionList>(`/submission/all/view/${userId}/user`, {
      responseType: 'json'
    });
  }

  getSubmission(submissionId: string) : Observable<SubmissionDTO> {
    return this.httpClient.get<SubmissionDTO>(`/submission/get/${submissionId}`, {
      responseType: 'json'
    });
  }


}
