import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TestCasesCsvService {

  constructor(
    private httpClient: HttpClient
  ) {}

  importCasesCSV(id: number, file: File) {
    const formData = new FormData();
    formData.append('file', file);

    return this.httpClient.post(`/task/${id}/cases/import`, formData, {
      responseType: 'text'
    });
  }

  exportCasesCSV(id: number): Observable<Blob> {
    const headers = new HttpHeaders({
      'Content-Type': 'text/csv',
      'Accept': 'text/csv'
    });

    return this.httpClient.get(`/task/${id}/cases/export`, {
      headers: headers,
      responseType: 'blob'
    });
  }

}
