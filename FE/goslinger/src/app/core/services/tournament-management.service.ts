import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {TournamentPageResponse} from "../models/tournament.models";
import {TaskList} from "../models/task.models";
import {ParticipantList} from "../models/user.models";

@Injectable({
  providedIn: 'root'
})
export class TournamentManagementService {

  constructor(
    private httpClient: HttpClient
  ) {}

  searchTaskByText(searchText: string, page: number, size: number): Observable<TournamentPageResponse> {
    const params = new HttpParams()
      .set('searchText', searchText)
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<TournamentPageResponse>('/tournament/search', {params});
  }

  getTournamentTasks(id: number): Observable<TaskList> {
    return this.httpClient.get(`/tournament/${id}/tasks`, {
      responseType: 'json'
    });
  }

  getTournamentParticipants(id: number): Observable<ParticipantList> {
    return this.httpClient.get(`/tournament/${id}/participants`, {
      responseType: 'json'
    });
  }

}
