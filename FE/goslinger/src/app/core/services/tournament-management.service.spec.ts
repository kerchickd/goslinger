import { TestBed } from '@angular/core/testing';

import { TournamentManagementService } from './tournament-management.service';

describe('TournamentManagementService', () => {
  let service: TournamentManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TournamentManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
