import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {TaskPageResponse} from "../models/task.models";

@Injectable({
  providedIn: 'root'
})
export class TaskManagementService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  searchTaskByText(searchText: string, page: number, size: number): Observable<TaskPageResponse> {
    const params = new HttpParams()
      .set('searchText', searchText)
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<TaskPageResponse>('/task/search', {params});
  }

}
