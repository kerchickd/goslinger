import { TestBed } from '@angular/core/testing';

import { TestCasesCsvService } from './test-cases-csv.service';

describe('TestCasesCsvService', () => {
  let service: TestCasesCsvService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestCasesCsvService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
