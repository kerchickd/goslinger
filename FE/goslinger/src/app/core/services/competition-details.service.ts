import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TournamentInfo, TournamentResults, TournamentTasks} from "../models/competition.models";

@Injectable({
  providedIn: 'root'
})
export class CompetitionDetailsService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getTournamentInfo(id: number): Observable<TournamentInfo> {
    return this.httpClient.get<TournamentInfo>(`/competition/${id}`, {
      responseType: 'json'
    });
  }

  getTournamentTasks(id: number): Observable<TournamentTasks> {
    return this.httpClient.get<TournamentTasks>(`/competition/${id}/tasks`, {
      responseType: 'json'
    });
  }

  getTournamentResults(id: number): Observable<TournamentResults> {
    return this.httpClient.get<TournamentResults>(`/competition/${id}/results`, {
      responseType: 'json'
    });
  }

}
