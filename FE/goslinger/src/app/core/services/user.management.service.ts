import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ProfileDTO, UpdateUserRequest} from "../models/user.models";
import {HttpClient, HttpParams} from "@angular/common/http";
import {UserPageResponse} from "../models/user.management.models";

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getUserProfile(id : number) : Observable<ProfileDTO> {
    return this.httpClient.get<ProfileDTO>('/user/view/' + id + '/profile', {
      responseType: 'json',
    });
  }

  public updateUserProfile(id: number, body: UpdateUserRequest) : Observable<ProfileDTO> {
    return this.httpClient.put<ProfileDTO>('/user/view/' + id +'/profile/update', body, {
      responseType: 'json',
    });
  }

  searchUserByText(searchText: string, page: number, size: number): Observable<UserPageResponse> {
    const params = new HttpParams()
      .set('searchText', searchText)
      .set('page', String(page))
      .set('size', String(size));
    return this.httpClient.get<UserPageResponse>('/user/search', { params });
  }

}
