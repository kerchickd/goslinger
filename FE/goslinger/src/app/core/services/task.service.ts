import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TaskDTO} from "../models/task.models";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getTaskById(id: number) : Observable<TaskDTO> {
    return this.httpClient.get<TaskDTO>('/task/' + id, {
      responseType: 'json',
    });
  }

  createTask(body: TaskDTO) : Observable<TaskDTO> {
    return this.httpClient.post<TaskDTO>('/task/create', body, {
      responseType: 'json',
    });
  }

  updateTask(id: number, body: TaskDTO) : Observable<TaskDTO> {
    return this.httpClient.put<TaskDTO>('/task/' + id + '/update', body, {
      responseType: 'json',
    });
  }

}
