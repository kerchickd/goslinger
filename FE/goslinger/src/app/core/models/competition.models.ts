export interface TournamentInfo {
  id?: number;
  title?: string;
  description?: string;
  startTime?: string;
  endTime?: string;
  public?: boolean;
  participants?: number;
  maxParticipants?: number;
  tasks?: number;
}

export interface TournamentTasks {
  tasks: TaskInfo[];
}

export interface TaskInfo {
  letter?: string;
  id?: number;
  title?: string;
  difficulty?: string;
  color?: string;
}

export interface TournamentResults {
  headers: string[];
  body: BodyResult[];
}

export interface BodyResult {
  id?: number;
  icon?: string;
  firstname?: string;
  lastname?: string;
  totalMark?: number;
  marks: number[];
}
