export interface SubmissionList {
  submissions: SubmissionRow[];
}

export interface SubmissionRow {
  id?: string;
  createdTime?: string;
  language?: string;
  mark?: number;
  taskTittle?: string;
  tournamentTitle?: string;
}

export interface SubmissionDTO {
  id?: string;
  createdTime?: string;
  sourceCode?: string;
  language: string;
  icon?: string;
  firstname?: string;
  lastname?: string;
  mark?: number;
  status?: number;
  averageExecutionDuration?: number;
  error?: string;
  taskTittle?: string;
  tournamentTitle?: string;
  verdict?: string;
}

export interface CodeDTO {
  code?: string;
  language?: string;
}
