import {PageResponse} from "./rating.models";

export interface TaskDTO {
  id?: number;
  title?: string;
  timeLimit?: number;
  memoryLimit?: number;
  problemStatement?: string;
  inputText?: string;
  outputText?: string;
  exampleInputData?: string;
  exampleOutputData?: string;
  difficulty?: string;
}

export interface TaskPageResponse {
  pageInfo: PageResponse;
  taskRows?: TaskRowResponse[];
}

export interface TaskRowResponse {
  id?: number;
  title?: string;
  timeLimit?: number;
  memoryLimit?: number;
  testCasesAmount?: number;
  difficulty?: string;
}

export interface TaskList {
  tasks?: TaskRowResponse[];
}
