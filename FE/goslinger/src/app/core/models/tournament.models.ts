import {PageResponse} from "./rating.models";

export interface TournamentDTO {
  id?: number;
  title?: string;
  description?: string;
  startTime?: Date;
  endTime?: Date;
  public?: boolean;
  password?: string;
  maxParticipants?: number;
}

export interface TournamentPageResponse {
  pageInfo: PageResponse;
  tournamentRows?: TournamentRowResponse[];
}

export interface TournamentRowResponse {
  id?: number;
  title?: string;
  public?: boolean;
  participants?: number;
  maxParticipants?: number;
  amountTasks?: number;
  startTime?: string;
  endTime?: string;
}

export interface PasswordRequest {
  password?: string;
}
