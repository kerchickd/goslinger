export interface RatingResponse {
  pageInfo: PageResponse;
  ratingList?: RatingRowResponse[];
}

export interface PageResponse {
  page: number;
  size: number;
  totalCount: number;
  totalPages: number;
}

export interface RatingRowResponse {
  position?: number;
  id?: number;
  firstname?: string;
  lastname?: string;
  icon?: string;
  age?: number;
  points?: number;
  solvedTasks?: number;
  tournamentParticipation?: number;
}
