import {UserRowResponse} from "./user.management.models";

export interface AuthenticationRequest {
  email?: string;
  password?: string;
}

export interface RegisterRequest {
  firstname?: string;
  lastname?: string;
  email?: string;
  password?: string;
  birthdayDate?: Date;
}

export interface ProfileDTO {
  firstname?: string;
  lastname?: string;
  email?: string;
  icon?: string;
  points?: number;
  solvedTasks?: number;
  tournamentParticipation?: number;
  age?: number;
  role?: string;
  enabled?: boolean;
  birthdayDate?: Date;
}

export interface UpdateCurrentUserRequest {
  firstname?: string;
  lastname?: string;
  birthdayDate?: Date;
}

export interface UpdateUserRequest {
  id?: number;
  firstname?: string;
  lastname?: string;
  birthdayDate?: Date;
  changeRoleToAdmin?: boolean;
  enabled?: boolean;
}

export interface ParticipantList {
  participants?: UserRowResponse[];
}
