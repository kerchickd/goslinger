import {PageResponse} from "./rating.models";

export interface UserPageResponse {
  pageInfo: PageResponse;
  userRows?: UserRowResponse[];
}

export interface UserRowResponse {
  id?: number;
  firstname?: string;
  lastname?: string;
  icon?: string;
  age?: number;
  email?: string;
  enabled?: boolean;
  role?: string;
}
