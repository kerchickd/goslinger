import {AfterViewChecked, AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {SubmissionService} from "../../core/services/submission.service";
import {SubmissionDTO} from "../../core/models/submission.models";
import {Location} from "@angular/common";
import {HighlightService} from "../../core/services/highlight.service";
import 'prismjs/prism';

declare var Prism: any;

@Component({
  selector: 'app-submission',
  templateUrl: './submission.component.html',
  styleUrls: ['./submission.component.css']
})
export class SubmissionComponent implements OnInit {

  id: string;
  lang : { [key: string]: string } = {
    "PYTHON" : "python",
    "C" : "c",
    "CPP" : "cpp",
    "JAVA" : "java",
    "GO" : "go",
    "CS" : "csharp",
    "KOTLIN" : "kotlin",
    "SCALA" : "scala",
    "RUST" : "rust",
    "RUBY" : "ruby",
    "HASKELL" : "hs",
    "SWIFT" : "swift"
  };
  submission: SubmissionDTO = {
    language: ''
  };

  constructor(
    private route: ActivatedRoute,
    private submissionService: SubmissionService,
    private location: Location,
    private highlightService: HighlightService
  ) {}

  ngOnInit(): void {
    Prism.highlightAll();
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.id = String(params.get('id'));
      }
    );
    this.getSubmission();
  }

  code: string;

  private getSubmission() {
    return this.submissionService.getSubmission(this.id).subscribe(
      (response: SubmissionDTO) => {
        this.submission = response;
        this.code = Prism.highlight(this.submission.sourceCode, Prism.languages[this.lang[response.language]], this.lang[response.language]);
      },
      (error) => {
        this.location.back();
      }
    )
  }
}
