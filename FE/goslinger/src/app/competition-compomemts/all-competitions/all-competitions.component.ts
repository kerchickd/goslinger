import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {TournamentPageResponse, TournamentRowResponse} from "../../core/models/tournament.models";
import {CompetitionService} from "../../core/services/competition.service";
import {MatDialog} from "@angular/material/dialog";
import {MessagePopUpComponent} from "../../pop-ups-components/message-pop-up/message-pop-up.component";
import {ParticipatePopUpComponent} from "../../pop-ups-components/participate-pop-up/participate-pop-up.component";

@Component({
  selector: 'app-all-competitions',
  templateUrl: './all-competitions.component.html',
  styleUrls: ['./all-competitions.component.css']
})
export class AllCompetitionsComponent implements AfterViewInit {

  updateElem: number = 0;
  inputValue: string = '';
  displayedColumnsAvailable: string[] = ["title", "access", "users", "start", "end", "participate"]
  displayedColumns: string[] = ["title", "access", "users", "start", "end"]

  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild('paginator1') paginator1!: MatPaginator;
  @ViewChild('paginator2') paginator2!: MatPaginator;
  pageSize: number = 7;
  pageIndex: number = 0;
  totalItems: number = 0;
  dataSourceAvailable: MatTableDataSource<TournamentRowResponse> = new MatTableDataSource<TournamentRowResponse>();

  constructor(
    private competitionService: CompetitionService,
    private dialog: MatDialog
  ) { }

  ngAfterViewInit(): void {
    this.updateAvailable();
  }

  updateAvailable() {
    const searchText = this.inputValue;
    const page = this.paginator.pageIndex;
    const size = this.paginator.pageSize;
    this.competitionService.searchTournamentAvailable(searchText, page, size).subscribe(
      (response: TournamentPageResponse) => {
        this.dataSourceAvailable = new MatTableDataSource<TournamentRowResponse>(response.tournamentRows);
        this.totalItems = response.pageInfo.totalCount;
      }
    )
  }

  updatePaticipated() {
    const searchText = this.inputValue;
    const page = this.paginator1.pageIndex;
    const size = this.paginator1.pageSize;
    this.competitionService.searchTournamentParticipated(searchText, page, size).subscribe(
      (response: TournamentPageResponse) => {
        this.dataSourceAvailable = new MatTableDataSource<TournamentRowResponse>(response.tournamentRows);
        this.totalItems = response.pageInfo.totalCount;
      }
    )
  }

  updateFinished() {
    const searchText = this.inputValue;
    const page = this.paginator2.pageIndex;
    const size = this.paginator2.pageSize;
    this.competitionService.searchTournamentFinished(searchText, page, size).subscribe(
      (response: TournamentPageResponse) => {
        this.dataSourceAvailable = new MatTableDataSource<TournamentRowResponse>(response.tournamentRows);
        this.totalItems = response.pageInfo.totalCount;
      }
    )
  }

  updateElem0() {
    this.updateElem = 0;
    this.resetValues();
    this.updateAvailable();
  }

  updateElem1() {
    this.updateElem = 1;
    this.resetValues();
    this.updatePaticipated();
  }

  updateElem2() {
    this.updateElem = 2;
    this.resetValues();
    this.updateFinished();
  }

  onInputChange() {
    if (this.updateElem === 0) {
      this.updateAvailable();
    } else if (this.updateElem === 1) {
      this.updatePaticipated();
    } else {
      this.updateFinished();
    }
  }

  onPageChange($event: PageEvent) {
    if (this.updateElem === 0) {
      this.updateAvailable();
    } else if (this.updateElem === 1) {
      this.updatePaticipated();
    } else {
      this.updateFinished();
    }
  }

  participate(idTournament: number, public1: boolean) {
    const dialogRef = this.dialog.open(ParticipatePopUpComponent, {
      width: '500px',
      data: {"id": idTournament, "public1": public1}
    });
    dialogRef.afterClosed().subscribe(
      () => {
        this.resetValues();
        this.updateAvailable();
      }
    )
  }

  private resetValues() {
    this.inputValue = '';
    this.pageIndex = 0;
    this.pageSize = 7;
    this.totalItems = 0;
  }
}
