import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Location} from "@angular/common";
import {CompetitionDetailsService} from "../../core/services/competition-details.service";
import {
  BodyResult,
  TaskInfo,
  TournamentInfo,
  TournamentResults,
  TournamentTasks
} from "../../core/models/competition.models";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-competition-details',
  templateUrl: './competition-details.component.html',
  styleUrls: ['./competition-details.component.css']
})
export class CompetitionDetailsComponent implements OnInit, AfterViewInit {

  idTour: number;
  tournament: TournamentInfo;
  updateElem: number = 0;
  displayedColumns: string[] = ["letter", "title", "difficulty"];
  displayedColumnsResults: string[] = [];
  dataSource: MatTableDataSource<TaskInfo> = new MatTableDataSource<TaskInfo>();
  dataSourceResults: MatTableDataSource<BodyResult> = new MatTableDataSource<BodyResult>();
  numbers: number[];

  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild('paginator1') paginator1!: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private competitionDetailsService: CompetitionDetailsService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.idTour = Number(params.get('id'));
      }
    );
    this.getTournamentInfo();
  }

  ngAfterViewInit(): void {
    this.getTournamentTasks();
  }

  getTournamentInfo() {
    this.competitionDetailsService.getTournamentInfo(this.idTour).subscribe(
      (response: TournamentInfo) => {
        this.tournament = response;
      },
      (error) => {
        this.location.back();
      }
    );
  }

  updateElem0() {
    this.updateElem = 0;
    this.getTournamentInfo();
    this.getTournamentTasks();
  }

  updateElem1() {
    this.updateElem = 1;
    this.getTournamentResult();
  }

  back() {
    this.location.back();
  }

  getTournamentTasks() {
    this.competitionDetailsService.getTournamentTasks(this.idTour).subscribe(
      (response: TournamentTasks) => {
        this.dataSource = new MatTableDataSource<TaskInfo>(response.tasks);
        this.dataSource.paginator = this.paginator;
      }
    )
  }

  getRange(difficulty: string) : number[] {
    let n: number = 1;
    if (difficulty === "HIGH") {
      n = 3;
    } else if (difficulty === "MEDIUM") {
      n = 2;
    }
    return Array(n).fill(0).map((_, index) => index + 1);
  }

  getTournamentResult() {
    this.competitionDetailsService.getTournamentResults(this.idTour).subscribe(
      (response: TournamentResults) => {
        this.displayedColumnsResults = response.headers;
        this.dataSourceResults = new MatTableDataSource<BodyResult>(response.body);
        this.dataSourceResults.paginator = this.paginator1;
        this.numbers = Array(this.displayedColumnsResults.length - 2).fill(0).map((x, i) => i);
      }
    )
  }
}
