import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {TaskDTO} from "../../core/models/task.models";
import {MatDialog} from "@angular/material/dialog";
import {TestCasePopUpComponent} from "../../pop-ups-components/test-case-pop-up/test-case-pop-up.component";

@Component({
  selector: 'app-create-task-components',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit {
  titleForm: FormControl = new FormControl('',
    [Validators.required]);
  memoryLimitForm: FormControl = new FormControl('',
    [Validators.required]);
  timeLimitForm: FormControl = new FormControl('',
    [Validators.required]);
  difficulty: string = "LOW";
  inputData: string = '';
  outputData: string = '';
  exampleInputData: string = '';
  exampleOutputData: string = '';
  statement: string = '';

  constructor(
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
  }

  getErrorMessage(): string {
    return 'Cannot be empty!';
  }

  isDisableButton(): boolean {
    return this.titleForm.invalid || this.memoryLimitForm.invalid || this.timeLimitForm.invalid;
  }

  createButton() {
    let task: TaskDTO = {
      title: this.titleForm.value,
      memoryLimit: this.memoryLimitForm.value,
      timeLimit: this.timeLimitForm.value,
      difficulty: this.difficulty,
      inputText: this.inputData,
      outputText: this.outputData,
      problemStatement: this.statement,
      exampleInputData: this.exampleInputData,
      exampleOutputData: this.exampleOutputData
    }

    const dialogRef = this.dialog.open(TestCasePopUpComponent, {
      width: '620px',
      data: task
    });

  }
}
