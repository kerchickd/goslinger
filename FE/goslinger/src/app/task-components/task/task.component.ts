import { Component, OnInit } from '@angular/core';
import {TaskDTO} from "../../core/models/task.models";
import {FormControl, Validators} from "@angular/forms";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Location} from "@angular/common";
import {TaskService} from "../../core/services/task.service";
import {MatDialog} from "@angular/material/dialog";
import {TestCasesCsvService} from "../../core/services/test-cases-csv.service";
import {MessagePopUpComponent} from "../../pop-ups-components/message-pop-up/message-pop-up.component";
import {saveAs} from "file-saver";
import {CompetitionService} from "../../core/services/competition.service";
import {CodeDTO, SubmissionDTO} from "../../core/models/submission.models";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  idTournament: number;
  idTask: number;
  task: TaskDTO;
  titleForm: FormControl = new FormControl('',
    [Validators.required]);
  memoryLimitForm: FormControl = new FormControl('',
    [Validators.required]);
  timeLimitForm: FormControl = new FormControl('',
    [Validators.required]);
  updateElem: number = 0;
  code: CodeDTO = {
    code: '',
    language: 'PYTHON'
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private competitionService: CompetitionService,
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.idTournament = Number(params.get('idTournament'));
        this.idTask = Number(params.get('idTask'));
      }
    );
    this.getTask();
  }

  getErrorMessage(): string {
    return 'Cannot be empty!';
  }

  getTask() {
    this.competitionService.getTaskDTO(this.idTournament, this.idTask).subscribe(
      (response: TaskDTO) => {
        this.fillFields(response);
      },
      (error) => {
        this.location.back();
      }
    );
  }

  fillFields(response: TaskDTO) {
    this.task = response;
    this.titleForm = new FormControl(this.task.title,
      [Validators.required]);
    this.memoryLimitForm = new FormControl(this.task.memoryLimit,
      [Validators.required]);
    this.timeLimitForm = new FormControl(this.task.timeLimit,
      [Validators.required]);
  }

  updateElem0() {
    this.updateElem = 0;
  }

  updateElem1() {
    this.updateElem = 1;
  }

  send() {
    this.competitionService.createAndSendSubmission(this.idTournament, this.idTask, this.code).subscribe(
      (response: SubmissionDTO) => {
        this.router.navigate([`/submission/${response.id}`]);
      }
    );
  }
}
