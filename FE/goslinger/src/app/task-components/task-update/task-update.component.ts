import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {TaskDTO} from "../../core/models/task.models";
import {Location} from "@angular/common";
import {TaskService} from "../../core/services/task.service";
import {TestCasesCsvService} from "../../core/services/test-cases-csv.service";
import {FormControl, Validators} from "@angular/forms";
import { saveAs } from 'file-saver';
import {MatDialog} from "@angular/material/dialog";
import {MessagePopUpComponent} from "../../pop-ups-components/message-pop-up/message-pop-up.component";

@Component({
  selector: 'app-task-components-update',
  templateUrl: './task-update.component.html',
  styleUrls: ['./task-update.component.css']
})
export class TaskUpdateComponent implements OnInit {

  id: number;
  task: TaskDTO;
  titleForm: FormControl = new FormControl('',
    [Validators.required]);
  memoryLimitForm: FormControl = new FormControl('',
    [Validators.required]);
  timeLimitForm: FormControl = new FormControl('',
    [Validators.required]);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private taskService: TaskService,
    private dialog: MatDialog,
    private testCaseService: TestCasesCsvService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.id = Number(params.get('id'));
      }
    );
    this.getTask();
  }

  getErrorMessage(): string {
    return 'Cannot be empty!';
  }

  isDisableButton(): boolean {
    return this.titleForm.invalid || this.memoryLimitForm.invalid || this.timeLimitForm.invalid;
  }

  getTask() {
    this.taskService.getTaskById(this.id).subscribe(
      (response: TaskDTO) => {
        this.fillFields(response);
      },
      (error) => {
        this.location.back();
      }
    );
  }

  createButton() {
    this.task.title = this.titleForm.value;
    this.task.memoryLimit = this.memoryLimitForm.value;
    this.task.timeLimit = this.timeLimitForm.value;
    this.taskService.updateTask(this.id, this.task).subscribe(
      (response: TaskDTO) => {
        this.fillFields(response);
        const dialogRef = this.dialog.open(MessagePopUpComponent, {
          width: '300px',
          data: 'Task was successfully updated!'
        });
      }
    );
  }

  private fillFields(response: TaskDTO) {
    this.task = response;
    this.titleForm = new FormControl(this.task.title,
      [Validators.required]);
    this.memoryLimitForm = new FormControl(this.task.memoryLimit,
      [Validators.required]);
    this.timeLimitForm = new FormControl(this.task.timeLimit,
      [Validators.required]);
  }

  back() {
    this.location.back();
  }

  export() {
    this.testCaseService.exportCasesCSV(this.id).subscribe(
      (response: Blob) => {
        const filename = 'test-cases.csv';
        saveAs(response, filename)
      },
      (error: any) => {
        const dialogRef = this.dialog.open(MessagePopUpComponent, {
          width: '500px',
          data: 'Error occurred while downloading CSV!'
        });
      }
    );
  }

  importFile($event: any) {
    let file : File = $event.target.files[0]
    this.testCaseService.importCasesCSV(this.id, file).subscribe(
      (response) => {
        const dialogRef = this.dialog.open(MessagePopUpComponent, {
          width: '500px',
          data: response
        })
      },
      (error: Error) => {
        const dialogRef = this.dialog.open(MessagePopUpComponent, {
          width: '500px',
          data: 'Error occurred while uploading CSV!'
        })
      });
  }
}
