import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {TaskManagementService} from "../../core/services/task-management.service";
import {MatTableDataSource} from "@angular/material/table";
import {UserPageResponse, UserRowResponse} from "../../core/models/user.management.models";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {TaskPageResponse, TaskRowResponse} from "../../core/models/task.models";
import {RatingRowResponse} from "../../core/models/rating.models";

@Component({
  selector: 'app-task-components-management',
  templateUrl: './task-management.component.html',
  styleUrls: ['./task-management.component.css']
})
export class TaskManagementComponent implements AfterViewInit {

  displayedColumns: string[] = ["id", "title", "difficulty", "testCasesAmount", "timeLimit", "memoryLimit"];
  dataSource : MatTableDataSource<TaskRowResponse> = new MatTableDataSource<TaskRowResponse>();
  pageSize: number = 7;
  pageIndex: number = 0;
  totalItems: number = 0;
  inputValue: string = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private taskManagementService: TaskManagementService
  ) {}

  ngAfterViewInit(): void {
    this.loadData();
  }

  loadData() {
    const searchText = this.inputValue;
    const page = this.paginator.pageIndex;
    const size = this.paginator.pageSize;

    this.taskManagementService.searchTaskByText(searchText, page, size).subscribe(
      (response: TaskPageResponse) => {
        this.dataSource = new MatTableDataSource<TaskRowResponse>(response.taskRows);
        this.totalItems = response.pageInfo.totalCount;
      });
  }

  onPageChange(event: PageEvent) {
    this.loadData();
  }

  onInputChange() {
    this.loadData();
  }

  getRange(difficulty: string) : number[] {
    let n: number = 1;
    if (difficulty === "HIGH") {
      n = 3;
    } else if (difficulty === "MEDIUM") {
      n = 2;
    }
    return Array(n).fill(0).map((_, index) => index + 1);
  }
}
