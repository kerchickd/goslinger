import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './persistent-components/home/home.component';
import { LoginComponent } from './auth-components/login/login.component';
import {AccessGuard} from "./access.guard";
import {RegisterComponent} from "./auth-components/register/register.component";
import {ProfileComponent} from "./auth-components/profile/profile.component";
import {RatingComponent} from "./users-components/rating/rating.component";
import {UserManagementComponent} from "./users-components/user-management/user-management.component";
import {ForbiddenComponent} from "./auth-components/forbidden/forbidden.component";
import {UserDetailsComponent} from "./users-components/user-details/user-details.component";
import {TaskManagementComponent} from "./task-components/task-management/task-management.component";
import {CreateTaskComponent} from "./task-components/create-task/create-task.component";
import {TaskUpdateComponent} from "./task-components/task-update/task-update.component";
import {
  TournamentManagementComponent
} from "./tournament-components/tournament-management/tournament-management.component";
import {TournamentUpdateComponent} from "./tournament-components/tournament-update/tournament-update.component";
import {TournamentCreateComponent} from "./tournament-components/tournament-create/tournament-create.component";
import {AllCompetitionsComponent} from "./competition-compomemts/all-competitions/all-competitions.component";
import {CompetitionDetailsComponent} from "./competition-compomemts/competition-details/competition-details.component";
import {SubmissionComponent} from "./competition-compomemts/submission/submission.component";
import {TaskComponent} from "./task-components/task/task.component";

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AccessGuard], data : {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'profile', component: ProfileComponent, canActivate: [AccessGuard], data : {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'rating', component: RatingComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'management/user', component: UserManagementComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'management/task', component: TaskManagementComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'management/task/:id/update', component: TaskUpdateComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'management/task/create', component: CreateTaskComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'management/tournament', component: TournamentManagementComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'management/tournament/:id/update', component: TournamentUpdateComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'management/tournament/create', component: TournamentCreateComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN']}},
  {path: 'user/:id/profile', component: UserDetailsComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'tournaments', component: AllCompetitionsComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'tournament/:id', component: CompetitionDetailsComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'submission/:id', component: SubmissionComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'tournament/:idTournament/task/:idTask', component: TaskComponent, canActivate: [AccessGuard], data: {roles:['ROLE_ADMIN', 'ROLE_USER']}},
  {path: 'forbidden', component: ForbiddenComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
