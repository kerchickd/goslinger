import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {UserAuthService} from "./core/services/user-auth.service";

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {

  constructor(private userAuthService: UserAuthService,
              private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const roles = route.data['roles'] as Array<string>;
    if (!this.userAuthService.isLoggedIn()) {
      this.router.navigate(['/login']);
      return false;
    }
    let role = this.userAuthService.getRole();
    if (typeof role === "string") {
      if (roles.includes(role)) {
        return true;
      } else {
        this.router.navigate(['/forbidden']);
        return false;
      }
    }
    this.router.navigate(['/login']);
    return false;
  }

}
