import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Location} from "@angular/common";
import {TournamentService} from "../../core/services/tournament.service";
import {TournamentManagementService} from "../../core/services/tournament-management.service";
import {TournamentDTO, TournamentPageResponse, TournamentRowResponse} from "../../core/models/tournament.models";
import {MessagePopUpComponent} from "../../pop-ups-components/message-pop-up/message-pop-up.component";
import {MatDialog} from "@angular/material/dialog";
import {MatTableDataSource} from "@angular/material/table";
import {UserRowResponse} from "../../core/models/user.management.models";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {ParticipantList} from "../../core/models/user.models";
import {TaskManagementService} from "../../core/services/task-management.service";
import {TaskList, TaskPageResponse, TaskRowResponse} from "../../core/models/task.models";

@Component({
  selector: 'app-tournament-components-update',
  templateUrl: './tournament-update.component.html',
  styleUrls: ['./tournament-update.component.css']
})
export class TournamentUpdateComponent implements OnInit {

  id: number;
  updateElem: number = 0;
  tournament: TournamentDTO;

  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild('paginator1') paginator1!: MatPaginator;
  @ViewChild('paginator2') paginator2!: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private tournamentService: TournamentService,
    private tournamentManagementService: TournamentManagementService,
    private dialog: MatDialog,
    private taskManagementService: TaskManagementService
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.id = Number(params.get('id'));
      }
    );
    this.updateDetailsPage();
  }

  updateElem0() {
    this.updateElem = 0;
    this.updateDetailsPage();
  }

  updateElem1() {
    this.updateElem = 1;
    this.updateTaskPage();
  }

  private updateDetailsPage() {
    this.tournamentService.getTournamentById(this.id).subscribe(
      (response: TournamentDTO) => {
        this.tournament = response;
      }
    );
    this.tournamentManagementService.getTournamentParticipants(this.id).subscribe(
      (response: ParticipantList) => {
        this.dataSource = new MatTableDataSource<UserRowResponse>(response.participants);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  back() {
    this.location.back();
  }

  onSubmit() {
    this.tournamentService.updateTournament(this.id, this.tournament).subscribe(
      (response: TournamentDTO) => {
        this.tournament = response;
        const dialogRef = this.dialog.open(MessagePopUpComponent, {
          width: '400px',
          data: 'Tournament was successfully updated!'
        });
      });
  }

  displayedColumns: string[] = ["user", "email", "remove"];
  dataSource : MatTableDataSource<UserRowResponse> = new MatTableDataSource<UserRowResponse>();

  displayedColumnsTasksAdd: string[] = ["title", "difficulty", "testCasesAmount", "add"];
  dataSourceTasksAdd : MatTableDataSource<TaskRowResponse> = new MatTableDataSource<TaskRowResponse>();
  pageSize: number = 7;
  pageIndex: number = 0;
  totalItems: number = 0;
  inputValue: string = '';

  displayedColumnsTasksRemove: string[] = ["title", "difficulty", "testCasesAmount", "remove"];
  dataSourceTasksRemove : MatTableDataSource<TaskRowResponse> = new MatTableDataSource<TaskRowResponse>();

  remove(idRemove: number) {
    this.tournamentService.removeParticipant(this.id, idRemove).subscribe(
      () => {
        this.updateDetailsPage();
      }
    )
  }

  private updateTaskPage() {
    const searchText = this.inputValue;
    const page = this.paginator1.pageIndex;
    const size = this.paginator1.pageSize;
    this.taskManagementService.searchTaskByText(searchText, page, size).subscribe(
      (response: TaskPageResponse) => {
        this.dataSourceTasksAdd = new MatTableDataSource<TaskRowResponse>(response.taskRows);
        this.totalItems = response.pageInfo.totalCount;
      }
    );
    this.tournamentManagementService.getTournamentTasks(this.id).subscribe(
      (response: TaskList) => {
        this.dataSourceTasksRemove = new MatTableDataSource<TaskRowResponse>(response.tasks);
        this.dataSourceTasksRemove.paginator = this.paginator2;
      }
    );
  }

  onPageChange($event: PageEvent) {
    this.updateTaskPage();
  }

  getRange(difficulty: string) : number[] {
    let n: number = 1;
    if (difficulty === "HIGH") {
      n = 3;
    } else if (difficulty === "MEDIUM") {
      n = 2;
    }
    return Array(n).fill(0).map((_, index) => index + 1);
  }

  addTask(idTask: number) {
    this.tournamentService.addTask(this.id, idTask).subscribe(
      () => {
        this.updateTaskPage();
      }
    );
  }

  onInputChange() {
    this.updateTaskPage();
  }

  removeTask(idTask: number) {
    this.tournamentService.removeTask(this.id, idTask).subscribe(
      () => {
        this.updateTaskPage();
      }
    );
  }
}
