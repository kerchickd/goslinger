import { Component, OnInit } from '@angular/core';
import {TournamentDTO} from "../../core/models/tournament.models";
import {Router} from "@angular/router";
import {TournamentService} from "../../core/services/tournament.service";
import {MessagePopUpComponent} from "../../pop-ups-components/message-pop-up/message-pop-up.component";
import {Location} from "@angular/common";

@Component({
  selector: 'app-tournament-components-create',
  templateUrl: './tournament-create.component.html',
  styleUrls: ['./tournament-create.component.css']
})
export class TournamentCreateComponent implements OnInit {

  tournament: TournamentDTO = {
    title: '',
    maxParticipants: 0,
    startTime: new Date(),
    endTime: new Date(),
    description: '',
    public: true,
    password: ''
  };

  constructor(
    private router: Router,
    private tournamentService: TournamentService,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  back() {
    this.location.back();
  }

  onSubmit() {
    this.tournamentService.createTournament(this.tournament).subscribe(
      (response: TournamentDTO) => {
        this.router.navigate(['/management/tournament/' + response.id + '/update']);
      });
  }

}
