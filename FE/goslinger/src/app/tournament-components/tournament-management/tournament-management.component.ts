import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {TournamentManagementService} from "../../core/services/tournament-management.service";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {TournamentPageResponse, TournamentRowResponse} from "../../core/models/tournament.models";
import {TaskPageResponse, TaskRowResponse} from "../../core/models/task.models";

@Component({
  selector: 'app-tournament-components-management',
  templateUrl: './tournament-management.component.html',
  styleUrls: ['./tournament-management.component.css']
})
export class TournamentManagementComponent implements AfterViewInit  {

  displayedColumns: string[] = ["id", "title", "access", "maxParticipants", "amountTasks"];
  dataSource : MatTableDataSource<TournamentRowResponse> = new MatTableDataSource<TournamentRowResponse>();
  pageSize: number = 7;
  pageIndex: number = 0;
  totalItems: number = 0;
  inputValue: string = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private tournamentManagementService: TournamentManagementService
  ) { }

  ngAfterViewInit(): void {
    this.loadData();
  }

  loadData() {
    const searchText = this.inputValue;
    const page = this.paginator.pageIndex;
    const size = this.paginator.pageSize;

    this.tournamentManagementService.searchTaskByText(searchText, page, size).subscribe(
      (response: TournamentPageResponse) => {
        this.dataSource = new MatTableDataSource<TournamentRowResponse>(response.tournamentRows);
        this.totalItems = response.pageInfo.totalCount;
      });
  }

  onPageChange(event: PageEvent) {
    this.loadData();
  }

  onInputChange() {
    this.loadData();
  }

}
