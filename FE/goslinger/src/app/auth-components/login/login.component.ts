import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserAuthService} from "../../core/services/user-auth.service";
import {AuthenticationRequest} from "../../core/models/user.models";
import {UserService} from "../../core/services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide: boolean = true;
  emailForm: FormControl = new FormControl('',
    [Validators.required,
      Validators.email]);
  passwordForm: FormControl = new FormControl('',
    [Validators.required,
      Validators.minLength(4)]);
  errorMessage: string = 'Invalid credentials or access blocked';
  hideErrorMessage: boolean = true;

  constructor(private router: Router,
              private userAuthService: UserAuthService,
              private userService: UserService) {
  }

  ngOnInit(): void {
   if (this.userAuthService.isLoggedIn()) {
      this.router.navigate(['/']);
   }
  }

  getEmailErrorMessage() {
    if (this.emailForm.hasError('required')) {
      return 'You must enter an email';
    }

    return this.emailForm.hasError('email') ? 'Not a valid email' : '';
  }

  getPasswordErrorMessage() {
    if (this.passwordForm.hasError('required')) {
      return 'You must enter a password';
    }

    return this.passwordForm.hasError('minlength') ? 'Password should be at least 4 characters long' : '';
  }

  onSubmit() {
    const loginData: AuthenticationRequest = {
      email: this.emailForm.value,
      password: this.passwordForm.value
    };
    this.userService.login(loginData).subscribe(
      (response : any) => {
        this.userAuthService.setToken(response.access_token);
        this.userAuthService.setEmail(response.user_email);
        this.userAuthService.setRole(response.user_role);

        this.router.navigate(['/']);
      },
      (err) => {
        this.hideErrorMessage = false;
        this.passwordForm.reset();
      }
    )
  }

  isDisabled() {
    return this.emailForm.invalid || this.passwordForm.invalid;
  }

  register() {
    this.router.navigate(['/register'])
  }
}
