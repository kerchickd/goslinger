import {AfterViewChecked, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../core/services/user.service";
import {ProfileDTO, UpdateCurrentUserRequest} from "../../core/models/user.models";
import {FormControl, Validators} from "@angular/forms";
import {HighlightService} from "../../core/services/highlight.service";
import {MatTableDataSource} from "@angular/material/table";
import {SubmissionList, SubmissionRow} from "../../core/models/submission.models";
import {MatPaginator} from "@angular/material/paginator";
import {SubmissionService} from "../../core/services/submission.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, AfterViewChecked {

  private highlighted: boolean = false
  profile : ProfileDTO = {};
  updateElem: number = 0;
  firstnameForm: FormControl = new FormControl('',
    [Validators.required]);
  lastnameForm: FormControl = new FormControl('',
    [Validators.required]);
  birthdayForm: FormControl = new FormControl('',
    [Validators.required]);
  dataSource: MatTableDataSource<SubmissionRow> = new MatTableDataSource<SubmissionRow>();
  displayedColumns: string[] = ["tournament", "task", "mark", "createdTime", "language"];

  @ViewChild('paginator') paginator!: MatPaginator;

  constructor(private router: Router,
              private userService: UserService,
              private highlightService: HighlightService,
              private submissionService: SubmissionService) { }

  ngOnInit(): void {
    this.getProfile();
  }

  ngAfterViewChecked() {
    if (!this.highlighted) {
      this.highlightService.highlightAll()
      this.highlighted = true
    }
  }

  getProfile() {
    this.userService.getCurrentUserProfile().subscribe(
      (response: ProfileDTO) => {
        this.profile = response;
      }
    )
  }

  updateElem0() {
    this.updateElem = 0;
  }

  updateElem1() {
    this.updateElem = 1;
    this.firstnameForm = new FormControl(this.profile.firstname,
      [Validators.required]);
    this.lastnameForm = new FormControl(this.profile.lastname,
      [Validators.required]);
    this.birthdayForm = new FormControl(this.profile.birthdayDate,
      [Validators.required]);
  }

  getFirstnameErrorMessage() {
    return 'You must enter a firstname';
  }

  getLastnameErrorMessage() {
    return 'You must enter a lastname';
  }

  getBirthdayErrorMessage() {
    return 'You must choose your birthday date';
  }

  isDisabled() : boolean {
    return this.firstnameForm.invalid
      || this.lastnameForm.invalid
      || this.birthdayForm.invalid;
  }

  onUpdate() {
    const updateUserRequest : UpdateCurrentUserRequest = {
      firstname : this.firstnameForm.value,
      lastname : this.lastnameForm.value,
      birthdayDate : this.birthdayForm.value
    }
    this.userService.updateCurrentUserProfile(updateUserRequest).subscribe(
      (response: ProfileDTO) => {
        this.updateElem = 0;
        this.profile = response;
      }
    );
  }

  updateElem2() {
    this.updateElem = 2;
    this.getSubmissionList();
  }

  getSubmissionList() {
    this.submissionService.getCurrentUserSubmissions().subscribe(
      (response: SubmissionList) => {
        this.dataSource = new MatTableDataSource<SubmissionRow>(response.submissions);
        this.dataSource.paginator = this.paginator;
      }
    )
  }
}
