import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserAuthService} from "../../core/services/user-auth.service";

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.css']
})
export class ForbiddenComponent implements OnInit {

  constructor(private router: Router,
              private userAuthService: UserAuthService
  ) {}

  ngOnInit(): void {
    if (!this.userAuthService.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
  }

}
