import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './persistent-components/header/header.component';
import { HomeComponent } from './persistent-components/home/home.component';
import { LoginComponent } from './auth-components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ApiInterceptor} from "./core/interceptors/api.interceptor";
import {TokenInterceptor} from "./core/interceptors/token.interceptor";
import { RegisterComponent } from './auth-components/register/register.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import { ProfileComponent } from './auth-components/profile/profile.component';
import {MatChipsModule} from "@angular/material/chips";
import {MatDividerModule} from "@angular/material/divider";
import { RatingComponent } from './users-components/rating/rating.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSelectModule} from "@angular/material/select";
import { UserManagementComponent } from './users-components/user-management/user-management.component';
import { ForbiddenComponent } from './auth-components/forbidden/forbidden.component';
import { UserDetailsComponent } from './users-components/user-details/user-details.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import { TaskManagementComponent } from './task-components/task-management/task-management.component';
import { CreateTaskComponent } from './task-components/create-task/create-task.component';
import { TestCasePopUpComponent } from './pop-ups-components/test-case-pop-up/test-case-pop-up.component';
import {MatDialogModule} from "@angular/material/dialog";
import { TaskUpdateComponent } from './task-components/task-update/task-update.component';
import { MessagePopUpComponent } from './pop-ups-components/message-pop-up/message-pop-up.component';
import { TournamentManagementComponent } from './tournament-components/tournament-management/tournament-management.component';
import { TournamentCreateComponent } from './tournament-components/tournament-create/tournament-create.component';
import { TournamentUpdateComponent } from './tournament-components/tournament-update/tournament-update.component';
import { AllCompetitionsComponent } from './competition-compomemts/all-competitions/all-competitions.component';
import { ParticipatePopUpComponent } from './pop-ups-components/participate-pop-up/participate-pop-up.component';
import { CompetitionDetailsComponent } from './competition-compomemts/competition-details/competition-details.component';
import {HighlightService} from "./core/services/highlight.service";
import { SubmissionComponent } from './competition-compomemts/submission/submission.component';
import { TaskComponent } from './task-components/task/task.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    RatingComponent,
    UserManagementComponent,
    ForbiddenComponent,
    UserDetailsComponent,
    TaskManagementComponent,
    CreateTaskComponent,
    TestCasePopUpComponent,
    TaskUpdateComponent,
    MessagePopUpComponent,
    TournamentManagementComponent,
    TournamentCreateComponent,
    TournamentUpdateComponent,
    AllCompetitionsComponent,
    ParticipatePopUpComponent,
    CompetitionDetailsComponent,
    SubmissionComponent,
    TaskComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatDividerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    FormsModule,
    MatCheckboxModule,
    MatDialogModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    HighlightService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
