import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipatePopUpComponent } from './participate-pop-up.component';

describe('ParticipatePopUpComponent', () => {
  let component: ParticipatePopUpComponent;
  let fixture: ComponentFixture<ParticipatePopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParticipatePopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipatePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
