import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CompetitionService} from "../../core/services/competition.service";
import {RegisterRequest} from "../../core/models/user.models";

export interface DataParticipate {
  id: number;
  public1: boolean
}

@Component({
  selector: 'app-participate-pop-up',
  templateUrl: './participate-pop-up.component.html',
  styleUrls: ['./participate-pop-up.component.css']
})
export class ParticipatePopUpComponent {
  hide: boolean = true;
  password: string = '';
  message: string = '';

  constructor(public dialogRef: MatDialogRef<ParticipatePopUpComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DataParticipate,
              private competitionService: CompetitionService) { }


  onNoClick() {
    this.dialogRef.close();
  }


  onOkClick() {
    let body: RegisterRequest = {
      password: this.password
    }
    this.competitionService.participate(this.data.id, body).subscribe(
      (response) => {
        if (response === 'SUCCESS' || response === 'ALREADY') {
          this.dialogRef.close();
        } else if (response === 'MAX') {
          this.message = 'Maximum amount of participants';
        } else if (response === 'WRONG') {
          this.message = 'Wrong password. Try again.';
        }
      }
    )
  }
}
