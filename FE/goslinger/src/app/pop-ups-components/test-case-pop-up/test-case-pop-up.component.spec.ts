import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCasePopUpComponent } from './test-case-pop-up.component';

describe('TestCasePopUpComponent', () => {
  let component: TestCasePopUpComponent;
  let fixture: ComponentFixture<TestCasePopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestCasePopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCasePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
