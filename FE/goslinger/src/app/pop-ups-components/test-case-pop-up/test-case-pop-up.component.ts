import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TaskService} from "../../core/services/task.service";
import {TestCasesCsvService} from "../../core/services/test-cases-csv.service";
import {TaskDTO} from "../../core/models/task.models";
import {switchMap} from "rxjs/operators";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-test-case-pop-up',
  templateUrl: './test-case-pop-up.component.html',
  styleUrls: ['./test-case-pop-up.component.css']
})
export class TestCasePopUpComponent {

  file: File;

  constructor(public dialogRef: MatDialogRef<TestCasePopUpComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TaskDTO,
              private taskService: TaskService,
              private testCasesService: TestCasesCsvService,
              private location: Location) {}

  onNoClick() {
    this.dialogRef.close();
  }


  importFile(event: any) {
    this.file = event.target.files[0]

    this.taskService.createTask(this.data).pipe(
      switchMap((response: TaskDTO) => {
        const id = response.id || 1;
        return this.testCasesService.importCasesCSV(id, this.file)
      })
    ).subscribe(
      (response) => {
        this.location.back();
      }
    );
  }


}
