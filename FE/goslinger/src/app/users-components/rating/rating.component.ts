import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RatingResponse, RatingRowResponse} from "../../core/models/rating.models";
import {RatingService} from "../../core/services/rating.service";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements AfterViewInit  {
  displayedColumns: string[] = ["position", "user", "age", "points", "solvedTasks", "tournamentParticipation"];
  dataSource : MatTableDataSource<RatingRowResponse> = new MatTableDataSource<RatingRowResponse>();
  pageSize: number = 8;
  pageIndex: number = 0;
  totalItems: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private ratingService : RatingService
  ) {
  }

  ngAfterViewInit() {
    this.loadData();
  }

  loadData() {
    const page = this.paginator.pageIndex;
    const size = this.paginator.pageSize;

    this.ratingService.getRatingPageList(page, size).subscribe(
      (response: RatingResponse) => {
      this.dataSource = new MatTableDataSource<RatingRowResponse>(response.ratingList);
      this.totalItems = response.pageInfo.totalCount;
    });
  }

  onPageChange(event: PageEvent) {
    this.loadData();
  }

}
