import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../core/services/user.service";
import {UserAuthService} from "../../core/services/user-auth.service";
import {MatTableDataSource} from "@angular/material/table";
import {UserPageResponse, UserRowResponse} from "../../core/models/user.management.models";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {UserManagementService} from "../../core/services/user.management.service";

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements AfterViewInit  {

  displayedColumns: string[] = ["id", "user", "email", "age", "enabled", "role"];
  dataSource : MatTableDataSource<UserRowResponse> = new MatTableDataSource<UserRowResponse>();
  pageSize: number = 8;
  pageIndex: number = 0;
  totalItems: number = 0;
  inputValue: string = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userManagementService : UserManagementService,
              private userService : UserService,
              private userAuthService : UserAuthService
  ) {}

  ngAfterViewInit(): void {
    this.loadData();
  }

  loadData() {
    const searchText = this.inputValue;
    const page = this.paginator.pageIndex;
    const size = this.paginator.pageSize;

    this.userManagementService.searchUserByText(searchText, page, size).subscribe(
      (response: UserPageResponse) => {
        this.dataSource = new MatTableDataSource<UserRowResponse>(response.userRows);
        this.totalItems = response.pageInfo.totalCount;
      });
  }

  onPageChange(event: PageEvent) {
    this.loadData();
  }

  onInputChange() {
    this.loadData();
  }
}
