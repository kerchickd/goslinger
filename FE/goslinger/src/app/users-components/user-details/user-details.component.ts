import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {UserAuthService} from "../../core/services/user-auth.service";
import {UserManagementService} from "../../core/services/user.management.service";
import {ProfileDTO, UpdateCurrentUserRequest, UpdateUserRequest} from "../../core/models/user.models";
import {Location} from "@angular/common";
import {FormControl, Validators} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";
import {SubmissionList, SubmissionRow} from "../../core/models/submission.models";
import {MatPaginator} from "@angular/material/paginator";
import {SubmissionService} from "../../core/services/submission.service";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  id: number;
  updateElem: number = 0;
  profile: ProfileDTO = {};
  firstnameForm: FormControl = new FormControl('',
    [Validators.required]);
  lastnameForm: FormControl = new FormControl('',
    [Validators.required]);
  birthdayForm: FormControl = new FormControl('',
    [Validators.required]);
  enabledForm: FormControl = new FormControl(false);
  selectedRole: string = "0";
  dataSource: MatTableDataSource<SubmissionRow> = new MatTableDataSource<SubmissionRow>();
  displayedColumns: string[] = ["tournament", "task", "mark", "createdTime", "language"];

  @ViewChild('paginator') paginator!: MatPaginator;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userAuthService: UserAuthService,
              private userManagementService: UserManagementService,
              private location: Location,
              private submissionService: SubmissionService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.id = Number(params.get('id'));
      }
    );
    this.getUserProfile();
  }

  getUserProfile() {
    this.userManagementService.getUserProfile(this.id).subscribe(
      (user: ProfileDTO) => {
        this.profile = user;
      },
      (error) => {
        this.location.back();
      }
    );
  }

  updateElem0() {
    this.updateElem = 0;
  }

  updateElem1(){
    this.updateElem = 1;
    this.firstnameForm = new FormControl(this.profile.firstname,
      [Validators.required]);
    this.lastnameForm = new FormControl(this.profile.lastname,
      [Validators.required]);
    this.birthdayForm = new FormControl(this.profile.birthdayDate,
      [Validators.required]);
    this.enabledForm = new FormControl(this.profile.enabled);
    this.selectedRole = "0";
  }

  getFirstnameErrorMessage() {
    return 'You must enter a firstname';
  }

  getLastnameErrorMessage() {
    return 'You must enter a lastname';
  }

  getBirthdayErrorMessage() {
    return 'You must choose your birthday date';
  }

  isDisabled() : boolean {
    return this.firstnameForm.invalid
      || this.lastnameForm.invalid
      || this.birthdayForm.invalid;
  }

  onUpdate() {
    let changeToAdmin: boolean = this.selectedRole === "1";
    const updateUserRequest : UpdateUserRequest = {
      firstname : this.firstnameForm.value,
      lastname : this.lastnameForm.value,
      birthdayDate : this.birthdayForm.value,
      enabled: this.enabledForm.value,
      changeRoleToAdmin: changeToAdmin
    }
    this.userManagementService.updateUserProfile(this.id, updateUserRequest).subscribe(
      (response: ProfileDTO) => {
        this.updateElem = 0;
        this.profile = response;
      }
    );
  }

  checkRoleForUpdate1() {
    return this.userAuthService.getRole() === "ROLE_ADMIN" && this.profile.role === "USER";
  }

  updateElem2() {
    this.updateElem = 2;
    this.getSubmissionList();
  }

  getSubmissionList() {
    this.submissionService.getUserSubmissions(this.id).subscribe(
      (response: SubmissionList) => {
        this.dataSource = new MatTableDataSource<SubmissionRow>(response.submissions);
        this.dataSource.paginator = this.paginator;
      }
    )
  }
}
